# Mapping LGBT+ Bristol

## Introduction

'Mapping LGBT+ Bristol' is a project funded by the  Arts and Humanities Research Council. A collaboration between 
OutStories Bristol, the  University of Bristol, Bristol City Council, Bristol Record Office and Freedom Youth. 
The project engaged Bristol's lesbian, gay, bisexual, transgender and other gender, sexual and romantic 
minority (LGBT+) communities in Bristol and its surrounds in researching, writing, mapping and disseminating 
LGBT history using digital tools.

## Source code

The project has a number of software repositories:

[OutStoriesWordPress](https://bitbucket.org/outstories/outstorieswordpress)
Provides WordPress related work for the project. Namely, a plugin that allows contributions of places and stories that 
can be harvested by Bristol City Council's ArcGIS system, a plugin that use code from OutStoriesJS to show map points
from the ArcGIS system within WordPress.

[OutStoriesJs] (https://bitbucket.org/outstories/outstoriesjs)
JavaScript, HTML and CSS to display information harvested from Bristol City Council's ArcGIS system.

[OutStoriesApp] (https://bitbucket.org/outstories/outstoriesapp)
An Apache Cordova project that wraps the OutStoriesJs code so that it can be deployed as an iOS and Android app.

[IconResize] (https://bitbucket.org/outstories/iconresize)
A Python utility script that lets us resize a large square image into smaller ones, i.e. icons and badges
needed for the app store.

## Contact

The code in the repositories was written by developers in Research IT team of the University of Bristol. For any
technical queries about the project, please contact research-it@bristol.ac.uk. For details on the project, please
visit http://outstoriesbristol.org.uk

## OutStoriesWordPress

This repository holds the WordPress related work for project, including:

* A contribution plugin that allows users to contribute places and media. The place data is held
  in custom database tables while the media is stored in the WordPress media library. The plugin
  provides an XML feed of places that have been approved by the OutStories team, so that Bristol
  City Council can harvest the data.
* A plugin that displays a map with data taken retrieved from Bristol City Council's 'Know Your Place'
* Theme assets so we can adapt the Suffusion theme used by OutStories to implement the design provided
  by Dirty Design.

### Development environment

#### WordPress environment

We are using Vagrant (https://www.vagrantup.com/) and VirtualBox (https://www.virtualbox.org/)
to provide a VM (https://github.com/Varying-Vagrant-Vagrants/VVV) with a WordPress installation.

Install two vagrant plugins:
    
    vagrant plugin install vagrant-hostsupdater
    vagrant plugin install vagrant-triggers
    
Clone the Varying-Vagrant-Vagrants project:

    git clone https://github.com/Varying-Vagrant-Vagrants/VVV.git outstories-vvv
    
From the directory you can create a VM (takes a while the first time):

    cd outstories-vvv
    vagrant up
    
This will update your hosts file and http://local.wordpress.dev/ will provide a WordPress installation. 
The www folder on your host machine will be mapped to /srv/www on the VM.

Create a file at the root of the project called config.properties, add a property called pluginDir
that points to the plugin directory in the WordPress installation, i.e. the shared location on Vagrant.
For example:

    pluginDir=/Users/mikejones/vagrant/outstories-vvv/www/wordpress-default/wp-content/plugins

#### npm and Grunt

The project uses npm and grunt. Install Node.js to get npm, or 
update npm if already installed.
 
    sudo npm install npm -g
    
Use npm to install grunt and other tools:

    sudo npm install
    
To copy the plugin(s) to WordPress development installation on the VM:

    grunt dev

To watch for changes in files and automatically copy them to the development environment:

    grunt watch

Package the contribution plugin as a zip for release:

    grunt dist-cont

Package the KYP plugin as a zip for release:

    grunt dist-kyp

Copy WordPress Assets to the KYP plugin

    grunt wp_assets