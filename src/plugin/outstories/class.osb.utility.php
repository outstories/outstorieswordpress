<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

class OsbUtility {

    public static function osb_is_moderator() {
        return current_user_can('publish_posts');
    }

    public static function osb_file_type($post) {

        $image_extension = array('jpg', 'jpeg', 'jpe', 'gif', 'png');

        $file = get_attached_file($post->ID);
        $check = wp_check_filetype($file);
        $type = $check['ext'];

        if (in_array($type, $image_extension)) {
            return 'image';
        }

        if (in_array($type, wp_get_audio_extensions())) {
            return 'audio';
        }

        if (in_array($type, wp_get_video_extensions())) {
            return 'video';
        }

        return 'other';
    }

    /**
     * In case we are not using 5.3
     *
     * @param $code
     */
    public static function osb_response_code($code) {
        if (!function_exists('http_response_code')) {
            header('X-PHP-Response-Code: '. $code, true, $code);
        } else {
            http_response_code($code);
        }
    }

    /**
     * Get the first para with tags stripped.
     *
     * @param $text - text that will have html
     * @return string - text of the first paragraph
     */
    public static function osb_first_para($text) {

        $text = htmlentities2($text);

        $htmlDoc = new DOMDocument('1.0', 'UTF-8');
        $htmlDoc->loadHTML(mb_convert_encoding($text, 'HTML-ENTITIES', 'UTF-8'));
        $paras = $htmlDoc->getElementsByTagName('p');

        return ($paras->length > 0) ? htmlspecialchars(wp_strip_all_tags($paras->item(0)->textContent)) : "";

    }

    /**
     * The Council is using the WE8ISO8859P1 (ISO/IEC 8859-1) character set; we are using UTF-8. Some content
     * is being pasted from Word or similar and it includes smart quotes which is causing encoding issues in
     * the feed that we get back from the Council. This allows us to replace smart characters with their
     * 'dumb' equivalent. For code, see http://toao.net/48-replacing-smart-quotes-and-em-dashes-in-mysql
     *
     * @param $text - the text we want to parse.
     * @return mixed - the string with characters replaced
     */
    public static function osb_convert($text) {

        $text = str_replace(
            array("\xe2\x80\x98", "\xe2\x80\x99", "\xe2\x80\x9c", "\xe2\x80\x9d", "\xe2\x80\x93", "\xe2\x80\x94",
                "\xe2\x80\xa6"),
            array("'", "'", '"', '"', '-', '--', '...'),
            $text);

        $text = str_replace(
            array(chr(145), chr(146), chr(147), chr(148), chr(150), chr(151), chr(133)),
            array("'", "'", '"', '"', '-', '--', '...'),
            $text);

        return $text;
    }
}