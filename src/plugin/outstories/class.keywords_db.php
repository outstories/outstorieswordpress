<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

class keywords_db {

    // table name
    private $_osb_keywords_table = "osb_keywords";
    private $_osb_keywords_place_table = "osb_keywords_place";

    function __construct() {
    }

    // ---------- SETUP

    /**
     * Create a table for holding data about places.
     */
    function osb_setup_db() {

        global $wpdb;

        $create_keywords_table =
            'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . $this->_osb_keywords_table . ' (
            `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `keyword` VARCHAR(100) NOT NULL UNIQUE
            );';

        $create_keywords_place_table =
            'CREATE TABLE IF NOT EXISTS ' . $wpdb->prefix . $this->_osb_keywords_place_table . ' (
             keyword_id INT(11) NOT NULL,
             place_id INT(11) NOT NULL,
             PRIMARY KEY  (keyword_id,place_id)
            );';

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($create_keywords_table);
        dbDelta($create_keywords_place_table);

    }

    // ---------- TEAR DOWN

    /**
     * Delete the table holding place data.
     */
    function osb_tear_down_db() {

        global $wpdb;
        $drop_keywords_place_table = 'DROP TABLE IF EXISTS ' . $wpdb->prefix . $this->_osb_keywords_place_table . ';';
        $drop_keywords_table = 'DROP TABLE IF EXISTS ' . $wpdb->prefix . $this->_osb_keywords_table . ';';
        $wpdb->query($drop_keywords_place_table);
        $wpdb->query($drop_keywords_table);
    }

    // --------- CRUD

    /**
     * Get all the keywords in the system.
     *
     * @return array
     */
    function osb_find_all_keywords() {

        global $wpdb;

        $all_keywords_query = 'SELECT id, keyword FROM ' . $wpdb->prefix . $this->_osb_keywords_table;

        return $wpdb->get_results($all_keywords_query, ARRAY_A);
    }

    /**
     * Get all the keywords associated for a place.
     *
     * @param $place_id - the ID for a place.
     * @return array - the keywords.
     */
    function osb_find_all_keywords_place($place_id) {

        global $wpdb;

        if (!empty($place_id)) {
            $all_keywords_places_query = 'select k.id, k.keyword from wp_osb_keywords k, wp_osb_keywords_place kp ' .
                'where kp.keyword_id = k.id and kp.place_id = %d';

            return $wpdb->get_results($wpdb->prepare($all_keywords_places_query, $place_id), ARRAY_A);

        }

        // no results, return an empty array
        return array();
    }

    /**
     * Disassociate a keyword with a place.
     *
     * @param $keyword_id - the ID for a keyword
     * @param $place_id = the ID for a place
     */
    function osb_delete_keyword_place($keyword_id, $place_id) {

        global $wpdb;

        $wpdb->delete($wpdb->prefix . $this->_osb_keywords_place_table,
            array('keyword_id' => $keyword_id, 'place_id' => $place_id),
            array('%d', '%d'));
    }

    /**
     * Associate an existing keyword with a place.
     *
     * @param $keyword_id - the ID of the keyword.
     * @param $place_id - the ID of the place
     * @return false|int
     */
    function osb_add_existing_keyword_place($keyword_id, $place_id) {

        global $wpdb;

        $val = $wpdb->insert($wpdb->prefix . $this->_osb_keywords_place_table,
            array('keyword_id' => $keyword_id, 'place_id' => $place_id),
            array('%d', '%d'));

        return $val;
    }

    /**
     * Add a new keyword and associate with a place.
     *
     * @param $keyword_label - the label of the keyword
     * @param $place_id - the place ID
     * @return array|bool
     */
    function osb_add_keyword_place($keyword_label, $place_id) {

        // add the keyword
        $val = $this->osb_add_keyword($keyword_label);

        // fails, bail now
        if ($val == false) {
            return false;
        };

        // succeeded, associate with the place
        $keyword_id = $val['keyword_id'];
        $val = $this->osb_add_existing_keyword_place($keyword_id, $place_id);

        // fails, bail now
        if ($val == false) {
            return false;
        };

        // succeeded, then return an array of data
        return array(array('keyword_id' => $keyword_id, 'keyword_label' => $keyword_label,
            'place_id' => $place_id));
    }

    /**
     * Add a keyword.
     *
     * @param $keyword_label - the label
     * @return array - associative array with id and label
     */
    function osb_add_keyword($keyword_label) {

        global $wpdb;

        // remove escaping from the POST - allow API escape
        $val = stripslashes($keyword_label);

        $wpdb->insert($wpdb->prefix . $this->_osb_keywords_table,
            array('keyword' => $val),
            array('%s'));

        return array('keyword_id' => $wpdb->insert_id, 'keyword_label' => $val);
    }

    /**
     * Delete a keyword.
     *
     * @param $keyword_id - the ID of a keyword.
     * @return false|int
     */
    function osb_remove_keyword($keyword_id) {

        global $wpdb;

        // remove place associations with the keyword
        $wpdb->delete($wpdb->prefix . $this->_osb_keywords_place_table, array('keyword_id' => $keyword_id),
            array('%d'));

        // delete the keyword
        $val = $wpdb->delete($wpdb->prefix . $this->_osb_keywords_table, array('id' => $keyword_id),
            array('%d'));

        return $val;
    }

}