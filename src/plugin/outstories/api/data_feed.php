<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

class DataFeedXml {

    private $_osb_places_db;
    private $_osb_keywords_db;

    public function __construct(){
        $this->_osb_places_db = new places_db();
        $this->_osb_keywords_db = new keywords_db();
    }

    public function execute() {

        // get the options
        $options = get_option('osb_options');

        // get the moderated places
        $places = $this->_osb_places_db->all_moderated_places();

        header('Content-Type: text/xml');
        echo '<?xml version="1.0" encoding="UTF-8"?>';
        echo '<places>';

        // iterate over each place
        foreach ($places as $place) {

            // get the keywords
            $keywords = $this->_osb_keywords_db->osb_find_all_keywords_place($place['id']);

            echo '<place>';
            echo '<title>' . htmlspecialchars(OsbUtility::osb_convert($place['title'])) . '</title>';
            echo '<id>osb/place/' . $place['id'] .'</id>';

            if ($options['allow_html_desc'] === 'yes') {
                $desc = $place['desc'];
                $f_para = osbUtility::osb_first_para($place['desc']);
                echo '<description>' . OsbUtility::osb_convert($f_para) .
                    '</description>';
                echo '<description_full><![CDATA[' . OsbUtility::osb_convert($desc) .
                    ']]></description_full>';
            } else {
                echo '<description>' . OsbUtility::osb_convert($place['desc']) . '</description>';
                echo '<description_full>' . OsbUtility::osb_convert($place['desc']) . '</description_full>';
            }
            echo '<coordinates>';
            echo '<latitude>' . $place['lat'] . '</latitude>';
            echo '<longitude>' . $place['lon'] . '</longitude>';
            echo '</coordinates>';
            echo '<date>';
            echo '<estimated>';
            echo '<year_estimate>' . $place['year_estimate'] . '</year_estimate>';
            echo '<month_estimate>' . $place['month_estimate'] . '</month_estimate>';
            echo '<day_estimate>' . $place['day_estimate'] . '</day_estimate>';
            echo '</estimated>';
            echo '<actual>';
            echo '<year>' . $place['year'] . '</year>';
            echo '<month>' . $place['month'] . '</month>';
            echo '<day>' . $place['day'] . '</day>';
            echo '</actual>';
            echo '</date>';
            echo '<to_date>';
            echo '<estimated>';
            echo '<year_estimate>' . $place['to_year_estimate'] . '</year_estimate>';
            echo '<month_estimate>' . $place['to_month_estimate'] . '</month_estimate>';
            echo '<day_estimate>' . $place['to_day_estimate'] . '</day_estimate>';
            echo '</estimated>';
            echo '<actual>';
            echo '<year>' . $place['to_year'] . '</year>';
            echo '<month>' . $place['to_month'] . '</month>';
            echo '<day>' . $place['to_day'] . '</day>';
            echo '</actual>';
            echo '</to_date>';


            if (count($keywords) > 0) {
                echo '<keywords>';
                foreach($keywords as $keyword) {
                    echo '<keyword>' . htmlspecialchars(OsbUtility::osb_convert($keyword['keyword'])) . '</keyword>';
                }
                echo '</keywords>';
            }

            echo '<media_list>';

            $media_list = $this->_osb_places_db->osb_media_place($place['id']);

            foreach ($media_list as $item) {

                // get the media id
                $media_id = $item['media_id'];
                $post = get_post($media_id);

                echo '<media>';
                echo '<id>osb/media/' . $media_id . '</id>';
                echo '<title>' . htmlspecialchars(OsbUtility::osb_convert($post->post_title)) . '</title>';
                echo '<description>' . htmlspecialchars(OsbUtility::osb_convert($post->post_content)) . '</description>';

                $type = OsbUtility::osb_file_type($post);
                if ($type === 'image') {
                    $alt = get_post_meta($post->ID, '_wp_attachment_image_alt', true);
                    echo '<alt>' . htmlspecialchars(OsbUtility::osb_convert($alt)) . '</alt>';
                }
                echo '<caption>' . htmlspecialchars(OsbUtility::osb_convert($post->post_excerpt)) . '</caption>';
                echo '<url>' . wp_get_attachment_url($media_id) . '</url>';
                echo '<mime_type>' . $post->post_mime_type . '</mime_type>';
                echo '</media>';
            }

            echo '</media_list>';
            echo '</place>';
        }

        echo '</places>';



        exit;
    }

}