<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

class KeywordsJsonApi {

    private $osb_place_id_param = 'osb_place_id';
    private $osb_keyword_id_param = 'osb_keyword_id';
    private $osb_keyword_label_param = 'osb_keyword_label';

    private $_osb_keywords_db;
    private $_osb_places_db;

    public function __construct() {
        $this->_osb_keywords_db = new keywords_db();
        $this->_osb_places_db = new places_db();
    }

    /**
     * Get a list of all keywords.
     */
    public function findAll() {

        $data = $this->_osb_keywords_db->osb_find_all_keywords();

        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    /**
     * Get all the keywords associated with a single place.
     */
    public function findAllForPlace() {

        if (!empty($_GET[$this->osb_place_id_param])) {
            if (ctype_digit(sanitize_text_field($_GET[$this->osb_place_id_param]))) {
                $data = $this->_osb_keywords_db->osb_find_all_keywords_place(sanitize_text_field($_GET[$this->osb_place_id_param]));
                header('Content-Type: application/json');
                echo json_encode($data);
            } else {
                OsbUtility::osb_response_code(400);
            }
        }

        exit;
    }

    /**
     * Add or delete a keyword.
     */
    public function keyword() {
        // DELETE - remove a keyword
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
            if (!empty($_GET[$this->osb_keyword_id_param])) {
                if (!OsbUtility::osb_is_moderator()) {
                    OsbUtility::osb_response_code(403);
                    exit;
                }
                $val = $this->_osb_keywords_db->osb_remove_keyword(sanitize_text_field($_GET[$this->osb_keyword_id_param]));
                ($val == false) ? OsbUtility::osb_response_code(500) : OsbUtility::osb_response_code(200);
                exit;
            }
            OsbUtility::osb_response_code(400);
            exit;
        } else if ($_SERVER['REQUEST_METHOD'] === 'POST') { // POST - associate a keyword with a place

            if (!OsbUtility::osb_is_moderator()) {
                OsbUtility::osb_response_code(403);
                exit;
            }

            if (empty($_POST[$this->osb_keyword_label_param])) {
                OsbUtility::osb_response_code(400);
                exit;
            }

            $val = $this->_osb_keywords_db->osb_add_keyword($_POST[$this->osb_keyword_label_param]);
            if ($val == false) {
                OsbUtility::osb_response_code(500);
            } else {
                header('Content-Type: application/json');
                OsbUtility::osb_response_code(200);
                echo json_encode($val);
            }
            exit;

        } else {
            OsbUtility::osb_response_code(405);
            exit;
        }
    }

    /**
     * Associate or disassociate a keyword with a place.
     */
    public function keyword_place() {

        // DELETE - disassociate a keyword from place
        if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
            if (!empty($_GET[$this->osb_place_id_param]) && !empty($_GET[$this->osb_keyword_id_param])) {
                if (!$this->can_modify_keyword_place(sanitize_text_field($_GET[$this->osb_place_id_param]))) {
                    OsbUtility::osb_response_code(403);
                    exit;
                }
                $this->_osb_keywords_db->osb_delete_keyword_place(sanitize_text_field($_GET[$this->osb_keyword_id_param]),
                    sanitize_text_field($_GET[$this->osb_place_id_param]));
                OsbUtility::osb_response_code(200);
                exit;
            }
            OsbUtility::osb_response_code(400);
            exit;
        } else if ($_SERVER['REQUEST_METHOD'] === 'POST') { // POST - associate a keyword with a place
            // Do we have a parameter with a place id
            if (!empty($_POST[$this->osb_place_id_param])) {

                if (!$this->can_modify_keyword_place(sanitize_text_field($_POST[$this->osb_place_id_param]))) {
                    OsbUtility::osb_response_code(403);
                    exit;
                }

                // keyword id? Associate a place with an existing id.
                if (!empty($_POST[$this->osb_keyword_id_param])) {
                    $val = $this->_osb_keywords_db->osb_add_existing_keyword_place(sanitize_text_field($_POST[$this->osb_keyword_id_param]),
                        sanitize_text_field($_POST[$this->osb_place_id_param]));
                    ($val == false) ? OsbUtility::osb_response_code(500) : OsbUtility::osb_response_code(201);
                    exit;
                    // keyword label? Create a new keyword and associate with a place.
                } elseif (!empty($_POST[$this->osb_keyword_label_param])) {
                    $val = $this->_osb_keywords_db->osb_add_keyword_place(sanitize_text_field($_POST[$this->osb_keyword_label_param]),
                        sanitize_text_field($_POST[$this->osb_place_id_param]));

                    if ($val == false) {
                        OsbUtility::osb_response_code(500);
                    } else {
                        header('Content-Type: application/json');
                        OsbUtility::osb_response_code(201);
                        echo json_encode($val);
                    }

                    exit;
                }
            }
            OsbUtility::osb_response_code(400);
            exit;
        } else {
            OsbUtility::osb_response_code(405);
            exit;
        }

    }

    /**
     * Can a user modify a keyword for a place?
     *
     * @param $place_id - the ID of a place
     * @return bool true if can edit
     */
    private function can_modify_keyword_place($place_id) {

        if (OsbUtility::osb_is_moderator()) return true;

        $data = $this->_osb_places_db->osb_get_place($place_id);

        return ($data['user_id'] == get_current_user_id());
    }

}



