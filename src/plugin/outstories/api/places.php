<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

class PlacesJsonApi {

    private $_osb_places_db;

    public function __construct(){
        $this->_osb_places_db = new places_db();
    }

    public function execute() {

        $data = $this->_osb_places_db->find_all_places();

        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

}



