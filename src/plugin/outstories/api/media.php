<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

/**
 * Class MediaJsonApi
 */
class MediaJsonApi {

    // object to access the database
    private $_osb_places_db;

    /**
     * MediaJsonApi constructor.
     */
    public function __construct() {
        $this->_osb_places_db = new places_db();
    }

    /**
     * Get the media for a place.
     *
     * This method makes multiple API calls which, in turn, make database calls.
     * Can we reduce the calls to get the same information?
     */
    public function media_for_place() {

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {

            $data = array();

            $media_list = $this->_osb_places_db->osb_media_place(sanitize_text_field($_GET['osb_place_id']));

            foreach ($media_list as $item) {

                // get the media id
                $media_id = $item['media_id'];

                $obj = array('id' => $media_id);

                // get the post data
                $post = get_post($media_id);

                $type = OsbUtility::osb_file_type($post);

                $obj['title'] = $post->post_title;
                $obj['desc'] = $post->post_content;
                $obj['type'] = $type;

                if ($type == 'image') {
                    $obj['uri'] = wp_get_attachment_image_src($media_id, 'medium');
                } else {
                    $obj['uri'] = wp_get_attachment_url($media_id);
                }

                $obj['mime_type'] = get_post_mime_type($media_id);

                array_push($data, $obj);
            }

            header('Content-Type: application/json');
            echo json_encode($data);

        } else {
            OsbUtility::osb_response_code(405);
        }

        exit;
    }
}



