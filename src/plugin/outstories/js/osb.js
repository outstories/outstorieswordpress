// ----------- Layers

osbMap = {

    // JSON endpoints to get data about places and keywords
    placesEndpoint: "/wp-admin/admin-ajax.php?action=places",
    keywordsEndpoint: "/wp-admin/admin-ajax.php?action=keywords",
    placeKeywordEndpoint: "/wp-admin/admin-ajax.php?action=keyword-place",
    placesKeywordEndpoint: "/wp-admin/admin-ajax.php?action=keywords-places&osb_place_id=",

    // unprocessed keyword data
    keywordRawData: [],

    // base URL of the page the map is situated
    baseURL: undefined,

    // reference to the LeafletJS map object
    map: undefined,

    // track the current tile layer
    currentTileLayer: undefined,

    // track the currently selected point
    selectedPoint: undefined,

    /**
     * Setup the map for interaction:
     *
     * 1) Display a default layer and handle layer selection.
     *
     * @param map - a LeafletJS map object.
     */
    setup: function (map) {

        this.map = map;

        // show the default map
        this.defaultLayer().addTo(this.map);

        // handle layer selection
        this.handleLayerSelect();

        var url = decodeURIComponent(jQuery("#osb-map-wrapper").attr("data-url"));

        if (url.indexOf('/', url.length - 1) !== -1) {
            url = url + '?';
        } else {
            url = url + '&';
        }

        osbMap.baseURL = url;
    },

    /**
     * Setup the map for viewing points.
     *
     * @param map - a LeafletJS map object.
     */
    setupViewMode: function (map) {

        this.setup(map);

        // display data
        this.allPlaces();

    },

    /**
     * Setup the map for adding a new place to the map.
     *
     *@param map - a LeafletJS map object.
     */
    setupContributeMode: function (map) {

        this.setup(map);

        this.keywords();

        // stop the add media button if data has been added
        jQuery('#osb-place-title:input, #osb-place-desc:input, #osb-date-section > p > select:input, ' +
            '#osb-date-section > p > input:input').change(function() {

            jQuery('#osb-add-media').on('click', function (e) {
                jQuery('#osb-media-msg').text('You have unsaved changes. Save before adding media');
                e.preventDefault();
            });

        });

        // no selected point by default
        osbMap.selectedPoint = null;

        // form has selected lat/lon? If so, add the marker to the map.
        var lat = jQuery('#osb-place-lat').val();
        var lon = jQuery('#osb-place-lon').val();

        // add the marker to the map
        if (lat.length > 0 && lon.length > 0) {
            osbMap.selectedPoint = L.marker(L.latLng(lat, lon), {draggable: true, icon: osbMap.chooseMarker()});
            osbMap.selectedPoint.addTo(osbMap.map);
            osbMap.map.panTo(L.latLng(lat, lon));
        }

        // handle clicking on the map
        this.map.on('click', function (e) {

            if (osbMap.selectedPoint !== null) {
                osbMap.map.removeLayer(osbMap.selectedPoint);
            }

            osbMap.selectedPoint = L.marker(e.latlng, {draggable: true, icon: osbMap.chooseMarker()});
            osbMap.selectedPoint.addTo(osbMap.map);
        });

        // remove point if the map is dagged
        this.map.on('dragstart', function () {
            if (osbMap.selectedPoint !== null) {
                osbMap.map.removeLayer(osbMap.selectedPoint);
            }
        });

        // handle the save/exit
        jQuery('#osb-place-submit, #osb-place-submit-exit').on('click', function (e) {

            // get the latitude and longitude
            if (osbMap.selectedPoint !== null) {
                jQuery("#osb-place-lat").val(osbMap.selectedPoint.getLatLng().lat);
                jQuery("#osb-place-lon").val(osbMap.selectedPoint.getLatLng().lng);
            }

            // track the currently selected layer id
            var sLayer = jQuery(".osb-layer-selected").first().attr('data-layer-id');
            jQuery("#osb-selected-layer").val(sLayer);

            // keep track of the zoom
            var zoomLevel = osbMap.map.getZoom();
            jQuery("#osb-layer-zoom").val(zoomLevel);

            // keep track of the current map center
            var center = osbMap.map.getCenter();
            jQuery("#osb-current-lat").val(center.lat);
            jQuery("#osb-current-lon").val(center.lng);

        });

        // handle a postcode lookup
        jQuery('#osb-postcode-submit').on("click", function (e) {
            e.preventDefault();
            osbMap.postCodeLookup();
        });

        // Issue #83 - return to the bottom of the page after saving
        var state = jQuery('#osb-place-form').attr('data-state');
        if (typeof state !== 'undefined') {
            if (state === 'save') {
                // update save message
                jQuery("#osb-message").text("Draft saved");
                jQuery("#osb-message-wrapper").show().delay(3000).fadeOut();
                // scroll down
                jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, 1000);
            }
        }

        // lookup postcode on enter
        jQuery('#osb-postcode-input').keydown(function(e) {
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode === 13) {
                osbMap.postCodeLookup();
            } else if (keycode === 9) {
                jQuery('#osb-postcode-submit').focus(function() {
                });
            }
        });

        // get keywords for a place
        this.keywordsForPlace();
    },

    // ---------- Tile layer selection and management

    /**
     * Available layers are in the DOM in a list. The default layer is the first
     * one in the list.
     *
     * @returns {*}
     */
    defaultLayer: function () {

        // find the list of layers and get the first
        var l = jQuery(".osb-layer-selected");
        l.addClass('osb-layer-selected');

        // return the default layer
        return this.layer(this.extractLayerData(l));
    },

    /**
     * Handle layer selection, i.e. change the displayed maps.
     */
    handleLayerSelect: function () {


        jQuery("#osb-layer-list").find("> li > a").on("click", function (e) {

            jQuery(this).parent().siblings().removeClass('osb-layer-selected');

            // grab the id of the selected layer

            var data = osbMap.extractLayerData(jQuery(this).parent());

            jQuery(this).parent().addClass('osb-layer-selected');

            var l = osbMap.layer(data);

            // remove existing layers from the map
            if (typeof osbMap.currentTileLayer !== "undefined") {
                osbMap.map.removeLayer(osbMap.currentTileLayer);
            }

            // add selected layer to map
            osbMap.currentTileLayer = l;
            l.addTo(osbMap.map);

            e.preventDefault();
        });
    },

    /**
     * Create a JSON object that represents a layer from data attributes.
     * @param node
     * @returns {*|{id, name, url, attribution}}
     */
    extractLayerData: function (node) {
        var id = node.attr("data-layer-id");
        var name = node.text();
        var attribution = node.attr("data-layer-attr");
        var url = node.attr("data-layer-url");
        return this.layerData(id, name, url, attribution);
    },

    /**
     *
     * @param id - the ID of a layer
     * @param name - the name of a layer
     * @param url - the URL of a layer
     * @param attribution -  the attribution details of a layer
     * @returns {{id: *, name: *, url: *, attribution: *}}
     */
    layerData: function (id, name, url, attribution) {
        return {
            id: id,
            name: name,
            url: url,
            attribution: attribution
        };
    },

    /**
     * Create a leafletjs tile layer.
     * @param data
     * @returns {*}
     */
    layer: function layer(data) {
        return L.tileLayer(data.url, {
            id: data.id,
            name: data.name,
            attribution: data.attribution
        });
    },

    // ----------- places data

    allPlaces: function () {

        if (jQuery('#osb-place-form').length === 0) {
            jQuery.get(this.placesEndpoint, function (data) {
                for (var i = 0; i < data.length; i++) {
                    var marker = osbMap.createMarker(data[i]);
                    marker.addTo(osbMap.map);
                }
            });
        }
    },

    createMarker: function (place) {

        // create a leaflet point
        var point = L.latLng(place.lat, place.lon);

        // create the marker object
        var marker = L.marker(point, {icon: this.chooseMarker(place.status)});
        //       marker.osbPlaceId =  place.id;

        // create popup information and bind to the marker
        var infoContent = '<div class="osb-map-popup"><h4 class="osb-map-popup-title">' + place.title +
            '</h4><div class="osb-map-popup-data"><a href="' + osbMap.baseURL + 'osb_action=view&osb_place_id=' + place.id + '">More ...</a></div></div>';
        var popup = L.popup().setContent(infoContent);
        marker.bindPopup(popup);

        return marker;
    },

    chooseMarker: function (status) {

        var url = '/wp-content/plugins/outstories/images/marker-yellow.png';

        if (typeof status !== 'undefined') {
            if (status === "1") {
                url = '/wp-content/plugins/outstories/images/marker-red.png';
            } else if (status === "2") {
                url = '/wp-content/plugins/outstories/images/marker-blue.png';
            }
        }

        return L.icon({
            iconUrl: url,
            iconSize: [32, 38]
        });
    },


    keywordsForPlace: function () {

        var id = jQuery("#osb-place-id");

        if (id.length > 0) {
            jQuery("#osb-keywords-wrapper").show();

            jQuery.get(this.placesKeywordEndpoint + id.val(), function (data) {
                var out = "<ul id='osb-keyword-list'>";
                for (var i = 0; i < data.length; i++) {
                    out += "<li class='osb-keyword' data-id='" + id.val() + ":" + data[i].id + "'>" + data[i].keyword + " <a class='osb-remove-key' href='#'>x</a></li>";
                }
                out += "</ul>";
                jQuery("#osb-keyword-place").append(out);

                osbMap.handleRemoveKeyword();
            });

            jQuery("#osb-new-keyword-add").off().on("click", function (e) {

                e.preventDefault();

                // get the keyword
                var val = jQuery('#osb-new-keyword').val();

                // if we have a keyword ...
                if (typeof val !== 'undefined') {

                    var obj = osbMap.isExistingKeyword(val);

                    if (obj.id === null) { // new label
                        jQuery.post(osbMap.placeKeywordEndpoint, {osb_place_id: id.val(), osb_keyword_label: obj.label})
                            .done(function (data) {
                                for (var i = 0; i < data.length; i++) {
                                    osbMap.addKeywordToList(data[i].place_id, data[i].keyword_id,
                                        data[i].keyword_label);
                                }
                                osbMap.handleRemoveKeyword();
                            });

                    } else { // exiting label
                        jQuery.post(osbMap.placeKeywordEndpoint, {osb_place_id: id.val(), osb_keyword_id: obj.id})
                            .done(function () {
                                osbMap.addKeywordToList(id.val(), obj.id, obj.label);
                                osbMap.handleRemoveKeyword();
                            });
                    }

                    jQuery('#osb-new-keyword').val('');

                }
            });
        }
    },

    keywords: function () {

        var bloodhoundKeywords = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('keyword'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: []
        });

        jQuery.get(this.keywordsEndpoint, function (data) {
            osbMap.keywordRawData = data;
            bloodhoundKeywords.add(osbMap.keywordRawData);
        });

        jQuery('#osb-keyword .typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name: 'keywords',
                displayKey: 'keyword',
                source: bloodhoundKeywords
            });
    },

    /**
     * Check the provided keyword to see of it already exists in the keyword list. It returns an object with an
     * id and keyword. If the id is null, it is a new keyword.
     *
     * @param val - a value provided bt the user.
     * @returns {{id: null, label: *}}
     */
    isExistingKeyword: function (val) {
        var hasVal = {id: null, label: val};
        jQuery.each(osbMap.keywordRawData, function (i, obj) {
            if (obj.keyword === val) {
                hasVal.id = obj.id;
                return false;
            }
        });
        return hasVal;
    },


    addKeywordToList: function (placeId, keywordId, label) {
        jQuery("#osb-keyword-list").append('<li class="osb-keyword" data-id="' + placeId + ':' + keywordId + '">' +
            label + ' <a class="osb-remove-key" href="">x</a></li>');
    },

    handleRemoveKeyword: function () {
        jQuery('a.osb-remove-key').off('click').on('click', function (e) {
            e.preventDefault();
            var data = jQuery(e.target).parent().attr('data-id');
            if (typeof data !== "undefined") {
                var tmp = data.split(":");

                if (tmp.length == 2) {
                    jQuery.ajax({
                        url: osbMap.placeKeywordEndpoint + '&osb_place_id=' + tmp[0] + '&osb_keyword_id=' + tmp[1],
                        type: 'DELETE',
                        success: function () {
                            // Do something with the result
                            jQuery(e.target).parent().remove();
                        }
                    });
                }
            }
        });
    },

    updateMessage: function (msg) {
        jQuery("#osb-message").text(msg);
        jQuery("#osb-message-wrapper").show().delay(3000).fadeOut();
    },

    postCodeLookup: function() {

        jQuery("#osb-postcode-msg").text("");

        var postcode = jQuery("#osb-postcode-input").val();

        if (postcode === "") {
            return;
        }

        jQuery.get('https://api.postcodes.io/postcodes/' + postcode, function (data) {
            if (data.status === 200) {
                if (osbMap.selectedPoint !== null) {
                    osbMap.map.removeLayer(osbMap.selectedPoint);
                }
                osbMap.selectedPoint = L.marker(L.latLng(data.result.latitude, data.result.longitude),
                    {draggable: true, icon: osbMap.chooseMarker()});
                osbMap.selectedPoint.addTo(osbMap.map);
                osbMap.map.panTo(L.latLng(data.result.latitude, data.result.longitude));
            }
        }).fail(function (e) {
            jQuery("#osb-postcode-msg").text(e.responseJSON.error);
        });
    }
};

osbMediaPlace = {

    mediaPlaceEndpoint: '/wp-admin/admin-ajax.php?action=media-place&osb_place_id=',

    setup: function () {

        var place_id = jQuery("div[data-place-id]");

        if (place_id.length > 0) {
            this.mediaForPlace(place_id.data('place-id'));
        }
    },

    mediaForPlace: function (placeId) {

        jQuery.get(this.mediaPlaceEndpoint + placeId, function (data) {

            if (data.length > 0) {

                var out = '<div id="osb-media-list-wrapper">';

                for (var i = 0; i < data.length; i++) {

                    out += '<div class="osb-media-item" data-media-id="' + data[i].id +  '">';
                    out += '<h4 class="osb-media-title">' + (i +1) + ') ' + data[i].title + '</h4>';
                    if (data[i].type == 'image') {
                        out += '<img src="' + data[i].uri[0] + '"/>';
                    } else if (data[i].type == 'audio') {
                        out += '<audio controls><source src="' + data[i].uri + '" type="' + data[i].mime_type +
                            '"/></audio>';
                    } else if (data[i].type == 'video') {
                        out += '<video controls><source src="' + data[i].uri + '" type="' + data[i].mime_type +
                            '"/></video>';
                    } else {
                        out += '<a href="' + data[i].uri + '" target="_blank">Media (' + data[i].mime_type + ')</a>';
                    }

                    out += data[i].desc;

                    out += ' <br/><span>[<a href="./?osb_action=view_media&osb_place_id=' + placeId + '&osb_media_id=' +
                        data[i].id + '">More</a>]';

                    out += '</div>';
                }

                out += '</div> <!-- .osb-media-list-wrapper -->';
                jQuery("#osb-media-wrapper").html(out);
            }

        });
    }
};
