<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

/**
 * Class Status
 *
 * Represents the status of submission to the plugin.
 */
abstract class Status {

    const Draft = 0;
    const Awaiting_Approval = 1;
    const Approved = 2;

}