<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

abstract class Date {

    // ---------- values
    private static $year_estimate = array('' => 'N/A', '1100|1199' => '12th Century', '1200|1299' => '13th Century',
        '1300|1399' => '14th Century', '1400|1499' => '15th Century', '1500|1599' => '16th Century',
        '1600|1699' => '17th Century', '1700|1799' => '18th Century', '1800|1899' => '19th Century',
        '1900|1949' => 'Early 20th Century', '1940|1949' => '1940s', '1950|1959' => '1950s',
        '1950|1999' => 'Late 20th Century', '1960|1969' => '1960s', '1970|1979' => '1970s',
        '1980|1989' => '1980s', '1990|1999' => '1990s', '2000|2049' => 'Early 21st Century');


    private static $month_estimate = array('' => 'N/A', '2|4' => 'Spring', '5|7' => 'Summer', '8|10' => 'Autumn',
        '11|1' => 'Winter');

    private static $day_estimate = array('' => 'N/A', '0|12' => 'The start of the month',
     '10|20' => 'The middle of the month', '18|31' => 'The end of the month');

    private static $month = array('' => 'N/A', '1' => 'January', '2' => 'February', '3' => 'March', '4' => 'April',
        '5' => 'May', '6' => 'June', '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October',
        '11' => 'November', '12' => 'December');


    // ---------- getter methods

    public static function YearEstimate() {
        return self::$year_estimate;
    }

    public static function MonthEstimate() {
        return self::$month_estimate;
    }

    public static function DayEstimate() {
        return self::$day_estimate;
    }

    public static function Month() {
        return self::$month;
    }
}