<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

// ---------- ADMIN PAGE

$osb_admin = new OsbAdmin();

class OsbAdmin {

    private $_osb_admin_capability = 'manage_options';

    private $_osb_main_title = 'Outstories';
    private $_osb_main_slug = 'osb-main-menu-page';
    private $_osb_main_func = 'osb_map_settings_page';

    function __construct() {
        add_action('admin_menu', array($this, 'osb_admin_menu'));
        add_action('admin_init', array($this, 'osb_admin_init'));
    }

    function osb_admin_menu() {

        add_menu_page($this->_osb_main_title, $this->_osb_main_title, $this->_osb_admin_capability,
            $this->_osb_main_slug, array($this , $this->_osb_main_func));

        add_submenu_page($this->_osb_main_slug, 'Keywords', 'Keywords',
            $this->_osb_admin_capability, 'osb-keywords-settings', array($this, 'osb_keywords_admin'));
    }


    function osb_map_settings_page() {
        $options = get_option('osb_options');
        ?>
        <div id="osb-general" class="wrap">
            <?php
            if (isset($_GET['message']) && sanitize_text_field($_GET['message']) == '1') { ?>
                <div id='message' class='updated fade'><p><strong>Settings Saved</strong></p></div>
            <?php } ?>

            <form method="post" action="admin-post.php">
                <input type="hidden" name="action"
                       value="save_osb_options"/>
                <!-- Adding security through hidden referrer field -->
                <?php wp_nonce_field('osb'); ?>
                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row">Default location</th>
                        <td>
                            <label for="osb_default_latitude">Latitude</label>
                            <input type="text" id="osb_default_latitude" name="osb_default_latitude"
                                   value="<?php echo esc_html($options['default_latitude']);
                                   ?>"/>
                            <label for="osb_default_longitude">Longitude</label>
                            <input type="text" id="osb_default_longitude" name="osb_default_longitude"
                                   value="<?php echo esc_html($options['default_longitude']);
                                   ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Default zoom</th>
                        <td>
                            <label for="osb_default_zoom">Zoom</label>
                            <input id="osb_default_zoom" name="osb_default_zoom" type="range" min="0" max="18"
                                   step="1"
                                   value="<?php echo esc_html($options['default_zoom']); ?>">
                            <span id="osb-zoom-val"><?php echo esc_html($options['default_zoom']) ?></span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Map layer list placement</th>
                        <td>
                            <label for="osb_layer_placement_above">Place above map</label>
                            <input id="osb_layer_placement_above" name="osb_layer_placement" value="above"
                                   type="radio" <?php if ($options['layer_placement'] == 'above') echo 'checked="checked"' ?>>
                            <label for="osb_layer_placement_below">Place below map</label>
                            <input id="osb_layer_placement_below" name="osb_layer_placement" value="below"
                                   type="radio" <?php if ($options['layer_placement'] == 'below') echo 'checked="checked"' ?>>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Display unmoderated content?</th>
                        <td>
                            <label for="osb_display_unmoderated_content_yes">Yes</label>
                            <input id="osb_display_unmoderated_content_yes" name="osb_display_unmoderated_content"
                                   value="yes"
                                   type="radio" <?php if ($options['display_unmoderated_content'] == 'yes') echo 'checked="checked"' ?>>
                            <label for="osb_display_unmoderated_content_no">No</label>
                            <input id="osb_display_unmoderated_content_no" name="osb_display_unmoderated_content"
                                   value="no"
                                   type="radio" <?php if ($options['display_unmoderated_content'] == 'no') echo 'checked="checked"' ?>>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Allow editing after moderation?</th>
                        <td>
                            <label for="osb_allow_edit_after_mod_yes">Yes</label>
                            <input id="osb_allow_edit_after_mod_yes" name="osb_edit_after_moderation"
                                   value="yes"
                                   type="radio" <?php if ($options['edit_after_moderation'] == 'yes') echo 'checked="checked"' ?>>
                            <label for="osb_allow_edit_after_mod_no">No</label>
                            <input id="osb_allow_edit_after_mod_no" name="osb_edit_after_moderation"
                                   value="no"
                                   type="radio" <?php if ($options['edit_after_moderation'] == 'no') echo 'checked="checked"' ?>>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Postcode lookup with Postcodes.io?</th>
                        <td>
                            <label for="osb_postcode_lookup_postcodes_io_yes">Yes</label>
                            <input id="osb_postcode_lookup_postcodes_io_yes" name="osb_postcode_lookup_postcodes_io"
                                   value="yes"
                                   type="radio" <?php if ($options['postcode_lookup_postcodes_io'] == 'yes') echo 'checked="checked"' ?>>
                            <label for="osb_postcode_lookup_postcodes_io_no">No</label>
                            <input id="osb_postcode_lookup_postcodes_io_no" name="osb_postcode_lookup_postcodes_io"
                                   value="no"
                                   type="radio" <?php if ($options['postcode_lookup_postcodes_io'] == 'no') echo 'checked="checked"' ?>>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Allow HTML for place descriptions?</th>allow_html_desc
                        <td>
                            <label for="osb_allow_html_desc">Yes</label>
                            <input id="osb_allow_html_desc_yes" name="osb_allow_html_desc" value="yes"
                                   type="radio" <?php if ($options['allow_html_desc'] == 'yes') echo 'checked="checked"' ?>>
                            <label for="osb_allow_html_desc_no">No</label>
                            <input id="osb_allow_html_desc_no" name="osb_allow_html_desc" value="no"
                                   type="radio" <?php if ($options['allow_html_desc'] == 'no') echo 'checked="checked"' ?>>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input type="submit" value="Submit"
                       class="button-primary"/>
            </form>
            <script type="application/javascript">
                jQuery('#osb_default_zoom').change(function () {
                    jQuery('#osb-zoom-val').text(jQuery(this).val());
                });
            </script>
        </div>
        <?php
    }

    function osb_keywords_admin() {

        global $wpdb;

        $result = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . "osb_keywords ORDER BY keyword", ARRAY_A);

        ?>
        <h2>Keywords</h2>
        <form action="post">
            <div>
                <ul id="osb-keyword-list">
                    <?php
                    foreach ($result as $row) {
                        echo '<li class="osb-keyword" data-id="' . $row['id'] . '">' . $row['keyword'] .
                            ' <a class="osb-remove-key" href="#">x</a></li>';
                    }
                    ?>
                </ul>
                <input id="osb-new-keyword" type="text"/><button id="submit-new-keyword">Add</button>
            </form>
        </div>
        <style>
            #osb-keyword-list {
                list-style: none;
            }
            #osb-keyword-list li {
                display: inline-block;
            }

            .osb-keyword {
                font-size: large;
                background-color: gray;
                margin: 0.3em;
                padding: 0.5em;
            }
        </style>
        <script type="application/javascript">

            var osb_admin_keywords = {

                setup: function() {
                    this.setupRemove();
                    this.submitClick();
                },

                setupRemove: function() {
                    jQuery('a.osb-remove-key').off('click').on('click', function(e) {

                        e.preventDefault();

                        var data = jQuery(e.target).parent().attr('data-id');

                        if (typeof data !== "undefined") {
                            jQuery.ajax({
                                url: '/wp-admin/admin-ajax.php?action=keyword&osb_keyword_id=' + data,
                                type: 'DELETE',
                                success: function() {
                                    // Do something with the result
                                    jQuery(e.target).parent().remove();
                                }
                            });
                        }
                    });
                },

                submit: function() {
                    var val =  jQuery("#osb-new-keyword").val();
                    console.log(val);
                    jQuery.post('/wp-admin/admin-ajax.php?action=keyword', { osb_keyword_label: val })
                        .done(function(data) {
                            jQuery("#osb-keyword-list").append('<li class="osb-keyword" data-id="' + data.keyword_id + '">' +
                                data.keyword_label + ' <a class="osb-remove-key" href="">x</a></li>');
                            osb_admin_keywords.setupRemove();
                            jQuery("#osb-new-keyword").val('');
                        });
                },

                submitClick: function() {
                    jQuery('#submit-new-keyword').off('click').on('click', function(e) {
                        e.preventDefault();
                        osb_admin_keywords.submit();
                    });
                }

            };

            osb_admin_keywords.setup();



        </script>
        <?php
    }

    function osb_admin_init() {
        add_action('admin_post_save_osb_options', array($this, 'process_osb_options'));
    }

    function process_osb_options() {

        if (!current_user_can($this->_osb_admin_capability))
            wp_die('Not allowed');

        check_admin_referer('osb');

        $options = get_option('osb_options');

        foreach (array('default_latitude', 'default_longitude', 'default_zoom', 'layer_placement',
                     'display_unmoderated_content', 'edit_after_moderation', 'postcode_lookup_postcodes_io',
                     'allow_html_desc') as $option_name) {
            if (isset($_POST['osb_' . $option_name])) {
                $options[$option_name] = sanitize_text_field($_POST['osb_' . $option_name]);
            }
        }

        update_option('osb_options', $options);

        $redirect = admin_url("admin.php?page=" . $this->_osb_main_slug . "&message=1");

        wp_redirect($redirect);

    }
}




