=== OutStories ===
Requires at least: 4.4
Tested up to: 4.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Changelog ==

= 1.0.7 =
* Disable adding media if we have unsaved changes
* Textual changes to the form

= 1.0.6 =
* On the data feed remove smart quotes etc. (Issue #94)

= 1.0.5 =
* Fixed a bug in the moderation feed so that it show the correct media for a place (Issue #93)

= 1.0.4 =
* Fixed some inefficient array handling
* Fixed encoding issues and array handling when getting the first paragraph (Issue #92)

= 1.0.3 =
* Enable a limited amount of HTML to be provided in a description for a place
* Update the feed so that KTP can harvest just the first paragraph

= 1.0.2 =
* Change the name of the buttons based on feedback (Issues #84 and #86)
* Initiate postcode search with the enter key (Issue #78)
* Change description storage from text to longtext (Issue #77)
* Support a date range (Issue #67)
* After saving a place, return to the bottom of the page (Issue #87)
* Provide feedback on the form (Issue #88)

= 1.0.1 =
* Provide a HTTP response is the http_response_code is not available. (Issue #72)
* Fixed some 'code smell' issues - unused parameter values.

= 1.0 =
* The initial public release.