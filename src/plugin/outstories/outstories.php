<?php
/*
  Plugin Name:     Outstories
  Plugin URI:      https://bitbucket.org/outstories/outstorieswordpressplugin#README
  Description:     Plugin for the AHRC-funded Outstories project.
  Version:         1.0.7
  Author:          Research IT, University of Bristol
  Author URI:      http://www.bristol.ac.uk/it-services/research/
  Licence:         GPLv2
  License URI:     https://www.gnu.org/licenses/gpl-2.0.html
*/

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

// ensure we are in a WordPress Blog
if (!defined('WP_PLUGIN_DIR')) {
    die('This is a WordPress plugin - I need WordPress!');
}

define('OSB__PLUGIN_URL', plugin_dir_url(__FILE__));
define('OSB__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('OSB_API_DIR', plugin_dir_path(__FILE__) . 'api/');

require_once(OSB_API_DIR . 'places.php');
require_once(OSB_API_DIR . 'keywords.php');
require_once(OSB_API_DIR . 'media.php');
require_once(OSB_API_DIR . 'data_feed.php');

require_once(OSB__PLUGIN_DIR . 'class.places_db.php');
require_once(OSB__PLUGIN_DIR . 'class.keywords_db.php');
require_once(OSB__PLUGIN_DIR . 'class.status.php');
require_once(OSB__PLUGIN_DIR . 'class.osb.utility.php');
require_once(OSB__PLUGIN_DIR . 'class.date.php');

if (is_admin()) {
    require plugin_dir_path(__FILE__) . 'outstories_admin.php';
}

$osb_map = new OSB_Map();

class OSB_Map {

    // database table of layer information
    protected $_osb_layer_table = "osb_layer_data";

    // object that provides db access
    private $_osb_places_db;
    private $_osb_keywords_db;

    // objects that handles json
    private $_osb_places_json_api;
    private $_osb_keywords_json_api;
    private $_osb_media_json_api;

    // objects that handle xml
    private $_osb_moderated_places_feed;

    private $_short_code_atts = null;

    private $_osb_mandatory_fields = array('osb-place-title', 'osb-place-lat', 'osb-place-desc');

    private $_osb_place_validation_messages = array(
        'osb-place-title' => 'You need to provide a title',
        'osb-place-lat' => 'You need to add a point on the map',
        'osb-place-desc' => 'You need to provide a description',
        'osb-date-range-mismatch' => 'You have provided the date for the end of a range, but not the start'
    );

    private $_osb_post_data_mapper = array(
        'osb-place-id' => 'id',
        'osb-place-title' => 'title',
        'osb-place-lat' => 'lat',
        'osb-place-lon' => 'lon',
        'osb-place-desc' => 'desc',
        'osb-place-year-estimate' => 'year_estimate',
        'osb-place-year' => 'year',
        'osb-place-month-estimate' => 'month_estimate',
        'osb-place-month' => 'month',
        'osb-place-day-estimate' => 'day_estimate',
        'osb-place-day' => 'day',
        'osb-place-to-year-estimate' => 'to_year_estimate',
        'osb-place-to-year' => 'to_year',
        'osb-place-to-month-estimate' => 'to_month_estimate',
        'osb-place-to-month' => 'to_month',
        'osb-place-to-day-estimate' => 'to_day_estimate',
        'osb-place-to-day' => 'to_day',
    );

    // parameter values
    private $_place_id_param = "osb_place_id";
    private $_media_id_param = "osb_media_id";
    private $_action_param = "osb_action";
    private $_contribute_action = "contribute";
    private $_view_action = "view";
    private $_public_action = "public";
    private $_add_media_action = "add_media";
    private $_edit_media_action = "edit_media";
    private $_view_media_action = "view_media";

    // form submission labels
    private $_place_save = "Save draft";
    private $_place_save_exit = "Safe draft and exit";
    private $_place_save_submit = "Submit for moderation";
    private $_place_exit = "Exit";
    private $_place_delete = "Delete";
    private $_place_approve = "Approve submission";
    private $_place_retract = "Retract approval";
    private $_media_save = "Save";
    private $_media_save_exit = "Save and exit";
    private $_media_delete = "Delete";
    private $_media_cancel = "Cancel";
    private $_media_view_done = "Return to place";
    private $_media_view_edit = "Edit";

    // ---------- CLASS CONSTRUCTOR

    /**
     *
     * OSB_Map constructor.
     */
    function __construct() {

        // objects for database access
        $this->_osb_places_db = new places_db();
        $this->_osb_keywords_db = new keywords_db();

        // objects that support Ajax requests
        $this->_osb_places_json_api = new PlacesJsonApi();
        $this->_osb_keywords_json_api = new KeywordsJsonApi();
        $this->_osb_media_json_api = new MediaJsonApi();

        // data feeds
        $this->_osb_moderated_places_feed = new DataFeedXml();

        // hook into activation and deactivation
        register_activation_hook(__FILE__, array($this, 'osb_activate_plugin'));
        register_deactivation_hook(__FILE__, array($this, 'osb_deactivate_plugin'));

        // hook into deleting media
        add_action('delete_attachment', array($this, 'delete_media_place'));

        // make sure scripts are available
        add_action('wp_enqueue_scripts', array($this, 'osb_custom_scripts'));

        // the map appears via a short code
        add_shortcode('osb_map', array($this, 'osb_short_code'));

        // register ajax to work with admin-ajax
        $this->register_ajax();
    }

    // ----------- PLUGIN SET UP

    /**
     * Register scripts and style sheets wih WordPress.
     */
    function osb_custom_scripts() {

        // make sure we have access to jquery
        wp_enqueue_script('jquery');

        // register the map leaflet javascript
        wp_register_script('leaflet-script', 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js');
        wp_register_style('leaflet-style', 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css');
        wp_enqueue_script('leaflet-script');
        wp_enqueue_style('leaflet-style');

        // typeahead library helps with Keywords
        wp_register_script('typeahead-script', plugins_url('/js/lib/typeahead.bundle.js', __FILE__));
        wp_register_style('typeahead-style', plugins_url('/css/lib/typeaheadjs.css', __FILE__));
        wp_enqueue_style('typeahead-style');
        wp_enqueue_script('typeahead-script');

        // register plugin css
        wp_register_style('osb-map-style', plugins_url('/css/osb.css', __FILE__));
        wp_enqueue_style('osb-map-style');

        // encode our JS file
        wp_register_script('osb-script', plugins_url('/js/osb.js', __FILE__));
        wp_enqueue_script('osb-script');

    }

    /**
     * Register some functions so that we can make public ajax calls to admin-ajax.
     */
    function register_ajax() {

        // get a list of places
        add_action('wp_ajax_places', array($this, 'osb_places_json'));
        add_action('wp_ajax_nopriv_places', array($this, 'osb_places_json'));

        // add or delete keywords
        add_action('wp_ajax_keyword', array($this, 'osb_keyword_json'));
        add_action('wp_ajax_nopriv_keyword', array($this, 'osb_keyword_json'));

        // get a list of keywords
        add_action('wp_ajax_keywords', array($this, 'osb_keywords_json'));
        add_action('wp_ajax_nopriv_keywords', array($this, 'osb_keywords_json'));

        // keywords for places
        add_action('wp_ajax_keywords-places', array($this, 'osb_keywords_places_json'));
        add_action('wp_ajax_nopriv_keywords-places', array($this, 'osb_keywords_places_json'));

        // associating keywords with a place
        add_action('wp_ajax_keyword-place', array($this, 'osb_keyword_place_json'));
        add_action('wp_ajax_nopriv_keyword-place', array($this, 'osb_keyword_place_json'));

        // media for a place
        add_action('wp_ajax_media-place', array($this, 'osb_media_place_json'));
        add_action('wp_ajax_nopriv_media-place', array($this, 'osb_media_place_json'));

        // moderated place feed
        add_action('wp_ajax_moderated-places', array($this, 'moderated_places_xml'));
        add_action('wp_ajax_nopriv_moderated-places', array($this, 'moderated_places_xml'));
    }

    // ---------- JSON DATA

    /**
     * Provide a list of places as a JSON feed.
     */
    function osb_places_json() {
        $this->_osb_places_json_api->execute();
    }

    /**
     * Keyword management.
     */
    function osb_keyword_json() {
        $this->_osb_keywords_json_api->keyword();
    }

    /**
     * Get a list of keywords.
     */
    function osb_keywords_json() {
        $this->_osb_keywords_json_api->findAll();
    }

    /**
     * Keywords for places.
     */
    function osb_keywords_places_json() {
        $this->_osb_keywords_json_api->findAllForPlace();
    }

    /**
     * Associating and disassociating keywords with places.
     */
    function osb_keyword_place_json() {
        $this->_osb_keywords_json_api->keyword_place();
    }

    /**
     * Media for a place
     */
    function osb_media_place_json() {
        $this->_osb_media_json_api->media_for_place();
    }

    // ---------- XML Data

    /**
     * XML feed of approved places.
     */
    function moderated_places_xml() {
        $this->_osb_moderated_places_feed->execute();
    }


    // ---------- SHORT CODE

    /**
     * Render stuff when the short code is parsed in a page.
     *
     * @param $atts - the short code attributes.
     * @return string - output from the plugin.
     */
    function osb_short_code($atts) {

        // keep track of the attributes
        $this->_short_code_atts = $atts;

        // the short code should just return a string, so we capture the
        // echo etc here and then return that
        ob_start();

        # start ui div
        ?>
        <div id='osb-ui'>
            <div id="osb-message-wrapper" style="display: none"><p id="osb-message"></p></div>
            <?php $this->controller() ?>
        </div><!-- .osb-ui -->

        <script type="application/javascript">
            var mapDiv = jQuery("#osb-map");
            if (mapDiv.length > 0) {
                var osbMapObj = L.map('osb-map').setView([mapDiv.attr("data-latitude"), mapDiv.attr("data-longitude")],
                    mapDiv.attr("data-zoom"));
                if (mapDiv.attr("data-mode") === "contribute") {
                    osbMap.setupContributeMode(osbMapObj);
                } else {
                    osbMap.setupViewMode(osbMapObj);
                }
            }

            var state = jQuery('#osb-map-wrapper').attr('data-state') || jQuery('#osb-place-details').attr('data-state');
            if (typeof state !== 'undefined') {
                if (state === 'submission_draft') {
                    // update save message
                    osbMap.updateMessage("Draft submission was saved.");
                    // scroll down
                    jQuery("html, body").animate({scrollTop: jQuery(document).height()}, 1000);
                } else if (state === 'submission_moderation') {
                    osbMap.updateMessage("Your submission has been submitted for moderation.");
                } else if (state == 'place_deleted') {
                    osbMap.updateMessage("The submission has been deleted.")
                } else if (state === 'submission_retracted') {
                    osbMap.updateMessage('Approval retracted from submission.');
                } else if (state === 'submission_approved') {
                    osbMap.updateMessage('Submission approved')
                }
            }
        </script>
        <?php

        return ob_get_clean();
    }

    /**
     * The "controller" decides what the plugin should be doing based on whether we are
     * dealing with a GET request or a POST request and the parameters received. It basically
     * coordinates the workflow of dealing with submissions to the map.
     */
    function controller() {

        // a POST - we must be processing data
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            if (!is_user_logged_in()) {
                $this->controller_error();
                return;
            }

            // a contribution (default is place)
            if (isset($_GET[$this->_action_param]) && sanitize_text_field($_GET[$this->_action_param]) == $this->_contribute_action) {
                $this->osb_handle_place_submit($_GET[$this->_action_param]);
            } else if (isset($_GET[$this->_action_param]) && sanitize_text_field($_GET[$this->_action_param]) == $this->_add_media_action) {
                if (!empty($_POST[$this->_media_id_param])) {
                    $this->osb_handle_media_metadata_submit();
                } else {
                    $this->osb_handle_media_submit();
                }
            } else if (isset($_GET[$this->_action_param]) && sanitize_text_field($_GET[$this->_action_param]) == $this->_edit_media_action) {
                $this->osb_handle_media_metadata_submit();
            }
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            if (count($_GET) == 0) {
                $this->osb_intro_text();
            }
            $this->process_get();
        } else {
            echo("<h3>I don't know what you want to do!</h3>");
        }

    }


    /**
     * Check the GET parameters so that we can work out what we need to do. Generally:
     *
     * 1) Add a place; 2) Edit a place; 3) Add media; 4) Edit media and 5) show a map.
     */
    function process_get() {

        if (!empty($_GET[$this->_action_param])) {

            // get the action parameter
            $action = sanitize_text_field($_GET[$this->_action_param]);

            if (($action === $this->_contribute_action || $action === $this->_add_media_action
                    || $action === $this->_edit_media_action) && !is_user_logged_in()
            ) {
                $this->controller_auth_error();
                return;
            }

            // contribute mode (add / edit a place)
            if ($action === $this->_contribute_action) {

                // existing place id? edit the place
                if (isset($_GET[$this->_place_id_param])) {

                    // edit place if we have an id
                    $place_id = sanitize_text_field($_GET[$this->_place_id_param]);
                    ctype_digit($place_id) ?
                        $this->osb_edit_place_view($place_id, $this->_contribute_action, array(), null) : $this->controller_error();

                } else {
                    $this->osb_edit_place_view(NULL, $this->_contribute_action, array(), null);
                }

                // view a place
            } else if ($action === $this->_view_action && isset($_GET[$this->_place_id_param])) {

                // view place if we have an id
                $place_id = sanitize_text_field($_GET[$this->_place_id_param]);
                ctype_digit($place_id) ? $this->osb_show_place($place_id) : $this->controller_error();

                // add media for a place
            } else if ($action === $this->_add_media_action) {

                if (isset($_GET[$this->_place_id_param])) {

                    // media form for place if we have an id
                    $place_id = sanitize_text_field($_GET[$this->_place_id_param]);
                    ctype_digit($place_id) ? $this->osb_media_form($place_id, null) : $this->controller_error();
                }

            } else if ($action === $this->_edit_media_action) {

                if (!empty($_GET[$this->_media_id_param]) && !empty($_GET[$this->_place_id_param])) {

                    $place_id = sanitize_text_field($_GET[$this->_place_id_param]);
                    $media_id = sanitize_text_field($_GET[$this->_media_id_param]);

                    (ctype_digit($place_id) && ctype_digit($media_id)) ?
                        $this->osb_media_metadata_form_page($place_id, $media_id) : $this->controller_error();

                }
            } else if ($action == $this->_view_media_action) {

                if (!empty($_GET[$this->_media_id_param]) && !empty($_GET[$this->_place_id_param])) {

                    $place_id = sanitize_text_field($_GET[$this->_place_id_param]);
                    $media_id = sanitize_text_field($_GET[$this->_media_id_param]);

                    (ctype_digit($place_id) && ctype_digit($media_id)) ?
                        $this->osb_show_media($place_id, $media_id) : $this->controller_error();
                }
            } else {
                $this->osb_map($this->_public_action);
            }
        } else {
            $this->osb_map($this->_public_action);
        }
    }

    /**
     * Process a POST request.
     */
    function process_post() {

        // a contribution (default is place)
        if (isset($_GET[$this->_action_param]) && sanitize_text_field($_GET[$this->_action_param]) === $this->_contribute_action) {
            $this->osb_handle_place_submit(sanitize_text_field($_GET[$this->_action_param]));
        } else if (isset($_GET[$this->_action_param]) && sanitize_text_field($_GET[$this->_action_param]) == $this->_add_media_action) {
            if (!empty($_POST[$this->_media_id_param])) {
                $this->osb_handle_media_metadata_submit();
            } else {
                $this->osb_handle_media_submit();
            }
        } else if (isset($_GET[$this->_action_param]) && sanitize_text_field($_GET[$this->_action_param]) == $this->_edit_media_action) {
            $this->osb_handle_media_metadata_submit();
        }
    }


    /**
     * Default error when we can't work out what we need to be doing.
     */
    function controller_error() {
        echo('<p class="osb-warning">Unexpected parameters. <a href="' . get_permalink() . '">Return to the map</a></p>');
    }

    /**
     * Default unauthorised error.
     */
    function controller_auth_error() {
        echo('<p class="osb-warning">Unauthorised. <a href="' . get_permalink() . '">Return to the map</a></p>');
    }


    // ---------- MAP

    /**
     * Displays a map.
     *
     * @param $mode - the map mode: 'public' for exploring and 'contribute' for adding, editing etc.
     * @param $state - keep track of state if needed.
     */
    function osb_map($mode, $state = null) {

        $out = '';

        if (!isset($mode)) {
            $mode = "public";
        }

        # short code attributes (with default values)
        $sc_atts = shortcode_atts(array(
            'width' => '300px',
            'height' => '300px',
        ), $this->_short_code_atts);

        # plugin options
        $options = get_option('osb_options');

        # start map wrapper
        $out .= '<div id="osb-map-wrapper" data-url="' . rawurlencode(get_page_link()) . '"';
        if (isset($state)) {
            $out .= ' data-state="' . $state . '"';
        }

        $out .= '>';

        # show layer list?
        if ($options['layer_placement'] == 'above') {
            $out .= $this->osb_layer_list();
        }

        $default_zoom = !empty($_POST['osb-layer-zoom']) ? sanitize_text_field($_POST['osb-layer-zoom']) : $options['default_zoom'];
        $default_lat = !empty($_POST['osb-current-lat']) ? sanitize_text_field($_POST['osb-current-lat']) : $options['default_latitude'];
        $default_lon = !empty($_POST['osb-current-lon']) ? sanitize_text_field($_POST['osb-current-lon']) : $options['default_longitude'];

        # map div
        $out .= '<div id="osb-map" data-mode="' . $mode . '" style="width: ' . $sc_atts['width'] . '; height: ' .
            $sc_atts['height'] . '" data-latitude="' .
            $default_lat . '" data-longitude="' . $default_lon . '" data-zoom="' .
            $default_zoom . '"></div>';

        # show layer list?
        if ($options['layer_placement'] == 'below') {
            $out .= $this->osb_layer_list();
        }

        # end map wrapper
        $out .= '</div><!-- .osb-map-wrapper -->';

        # contribution
        if ($mode == "public") {
            $out .= $this->osb_contribute_text();
        }

        echo($out);
    }


    // ---------- Place Submission handling

    /**
     * Handle the submission of a place. We have a number of submit buttons and we use these
     * to determine where we are heading.
     *
     * @param $mode - the map mode: 'public' for exploring and 'contribute' for adding, editing etc.
     */
    function osb_handle_place_submit($mode) {

        if (isset($_POST['osb-place-submit'])) {

            $submit = sanitize_text_field($_POST['osb-place-submit']);

            switch ($submit) {
                case $this->_place_exit:
                    $this->osb_handle_place_cancel();
                    break;
                case $this->_place_save:
                    $this->osb_handle_place_save($mode, $submit);
                    break;
                case $this->_place_save_exit:
                    $this->osb_handle_place_save($mode, $submit);
                    break;
                case $this->_place_delete;
                    $this->osb_handle_place_delete();
                    break;
                case $this->_place_save_submit:
                    $this->osb_handle_place_save($mode, $submit);
                    break;
                case $this->_place_approve;
                    $this->osb_handle_place_moderation($submit);
                    break;
                case $this->_place_retract;
                    $this->osb_handle_place_moderation($submit);
                    break;
                default:
                    $this->osb_handle_place_fallback($submit);
            }

            return;
        }

        error_log('The place form was submitted, but there was no \'osb-place-submit\' variable in the POST.' .
            'Showing the map, since I don\'t know what else to do');

        $this->osb_map("public");
    }

    /**
     * Save the place details. Ultimately an insert or an update into the database.
     *
     * @param $mode - the map mode: 'public' for exploring and 'contribute' for adding, editing etc.
     */
    function osb_handle_place_save($mode, $submit) {

        // check we have expected values
        $invalid = $this->osb_check_input($this->_osb_mandatory_fields);

        if ($this->osb_date_range_check()) {
            array_push($invalid, 'osb-date-range-mismatch');
        }

        // redisplay form if we have validation issues
        if (count($invalid) > 0) {
            $this->osb_edit_place_view(null, $mode, $invalid, null);
            return;
        }

        // wrap the post data
        $data = $this->osb_place_wrap_post_data($this->osb_place_data_wrapper(), $this->_osb_post_data_mapper);


        if (!empty($data['year']) && preg_match("/[^0-9]/", $data['year'])) {
            $data['year'] = "";
        }

        // get the user id if not set
        if (!isset($data['user_id'])) {
            $data['user_id'] = get_current_user_id();
        }

        // save
        $id = $this->_osb_places_db->osb_save_place($data);

        // "Save" re-displays form; "Done" displays place information.
        if ($submit == $this->_place_save) {
            $this->osb_edit_place_view($id, $mode, array(), 'save');
        } elseif ($submit == $this->_place_save_exit) {
            $this->osb_map("public", "submission_draft");
        } elseif ($submit == $this->_place_save_submit) {

            // change the status to done
            $this->_osb_places_db->osb_change_status($data['id'], Status::Awaiting_Approval);

            // show the place
            $this->osb_show_place($data['id'], 'submission_moderation');
        }
    }

    /**
     * Cancel place details submission - if we have a place id display that place, otherwise return to the map.
     */
    function osb_handle_place_cancel() {

        isset($_POST['osb-place-id']) ? $this->osb_show_place(sanitize_text_field($_POST['osb-place-id'])) : $this->osb_map("public");
    }

    /**
     * Delete place details - then return to the map.
     */
    function osb_handle_place_delete() {
        if (!empty($_POST['osb-place-id'])) {

            // get the ID
            $place_id = sanitize_text_field($_POST['osb-place-id']);

            // get the media attached to a place
            $media = $this->_osb_places_db->osb_media_place($place_id);

            // delete the media
            foreach ($media as $item) {
                wp_delete_attachment($item['media_id']);
            }

            // delete the place
            $this->_osb_places_db->osb_delete_place($place_id);
        }

        $this->osb_map("public", "place_deleted");
    }

    /**
     * We are either approving a place, or retracting approval.
     *
     * @param $submit - the value of the submit button from the form.
     */
    function osb_handle_place_moderation($submit) {

        if (isset($_POST['osb-place-id'])) {

            $state = null;

            if (OsbUtility::osb_is_moderator()) {
                $_approve = $submit == $this->_place_approve ? Status::Approved : Status::Awaiting_Approval;
                $state = ($_approve == 1) ? 'submission_retracted' : 'submission_approved';
                $this->_osb_places_db->osb_change_status(sanitize_text_field($_POST['osb-place-id']), $_approve);
            }

            $this->osb_show_place(sanitize_text_field($_POST['osb-place-id']), $state);
        }
    }

    /**
     * Fallback if we don't know what we are supposed to be doing after a form submission!
     *
     * @param $submit - the value of the submit button from the form.
     */
    function osb_handle_place_fallback($submit) {
        error_log('The place form was submitted, but received the unrecognised variable \'' . $submit .
            '\' in the POST. Showing the map, since I don\'t know what else to do!');
        $this->osb_map("public");
    }

    // ----------- Views generated by the plugin (and helper functions)

    /**
     * Display the information about a place in "view" mode.
     *
     * @param $id - the ID of a place.
     * @param $state - some state we need to track.
     */
    function osb_show_place($id, $state = null) {

        $data = $this->_osb_places_db->osb_get_place($id);

        if (!empty($data['user_id'])) {
            $user_info = get_userdata($data['user_id']);
            echo '<p>Added by ';
            if (!empty($user_info->first_name) || !empty($user_info->last_name)) {
                echo $user_info->first_name . ' ' . $user_info->last_name;
            } else {
                echo $user_info->user_login;
            }
            echo '</p>';
        }

        $keywords = $this->_osb_keywords_db->osb_find_all_keywords_place($id)

        ?>
        <div id="osb-place-details"<?php if (!empty($id)) {
            echo ' data-place-id="' . $id . '"';
        }
        if (isset($state)) {
            echo ' data-state="' . $state . '"';
        } ?>>

            <!-- Title -->
            <h3 class="osb-place-title"><?php echo $data['title'] ?></h3>

            <div><p>For technical reasons, <em>Know Your Place</em> cannot display multiple paragraphs in your
                    description. If you enter two or more paragraphs, the map on the OutStories Bristol website will
                    display the full text, but Know Your Place will only display the first paragraph.</p></div>

            <!-- Description (KYP)-->
            <div class="osb-place-description" class="osb-desc-border"><p><strong>Description (Know Your Place):</strong></p>

                <?php echo OsbUtility::osb_first_para($data['desc']) ?>

            </div><!-- .sb-place-description -->

            <!-- Description (full)-->
            <div class="osb-place-description" class="osb-desc-border"><p><strong>Description (full):</strong></p>

                <?php echo $data['desc'] ?>

            </div><!-- .sb-place-description -->

            <!-- Dates --->
            <div class="osb-place-date">
                <?php

                $exact = array();

                if (!empty($data['day'])) {
                    array_push($exact, $data['day']);
                }

                if (!empty($data['month'])) {
                    $months = Date::Month();
                    array_push($exact, $months[$data['month']]);
                }

                if (!empty($data['year'])) {
                    array_push($exact, $data['year']);
                }

                if (count($exact) > 0) {
                    echo '<p>';
                    foreach ($exact as $item) {
                        echo $item . '&nbsp';
                    }
                    echo '</p>';
                }

                $estimate = array();

                if (!empty($data['day_estimate']) && $data['day_estimate'] !== 'N/A') {
                    $days = Date::DayEstimate();
                    array_push($estimate, $days[$data['day_estimate']]);
                }

                if (!empty($data['month_estimate']) && $data['month_estimate'] !== 'N/A') {
                    $months = Date::MonthEstimate();
                    array_push($estimate, $months[$data['month_estimate']]);
                }

                if (!empty($data['year_estimate']) && $data['year_estimate'] !== 'N/A') {
                    $years = Date::YearEstimate();
                    array_push($estimate, $years[$data['year_estimate']]);
                }

                if (count($exact) > 0) {
                    echo '<p>';
                    foreach ($estimate as $item) {
                        echo $item . '<br/>';
                    }
                    echo '</p>';
                }
                ?>

            </div><!--- .osb-place-date -->

            <!-- Keywords -->
            <div class="osb-place-keywords">
                <p><strong>Keywords:</strong>
                    <?php
                    $i = 0;
                    foreach ($keywords as $index => $keyword) {
                        echo '<span>' . $keyword['keyword'] . '</span>';
                        if ($i != count($keywords) - 1) {
                            echo ', ';
                        }
                        $i++;
                    }
                    ?>
                </p>

            </div><!-- .sb-place-keywords -->

            <div id='osb-media-wrapper'>
                <div id='osb-media-list-wrapper'>

                </div>
            </div>

            <!-- Status -->
            <p><strong>Status:</strong>&nbsp;
                <?php

                switch ($data['status']) {
                    case Status::Draft:
                        echo 'This submission is a draft.';
                        break;
                    case Status::Awaiting_Approval:
                        echo 'This submission is awaiting moderation.';
                        break;
                    case Status::Approved:
                        echo 'This submission has been approved.';
                        break;
                    default:
                        echo 'Unknown status.';
                }
                ?>
            </p>
        </div><!-- .os-place-details -->

        <script type="application/javascript">
            osbMediaPlace.setup();
        </script>

        <?php


        if ($this->osb_allow_editing($data['status'], $data['user_id'])) {
            $edit_url = add_query_arg(array($this->_action_param => $this->_contribute_action,
                $this->_place_id_param => $data['id']));
            echo('<a class="osb-btn" href="' . esc_url($edit_url) . '">Edit</a>');
        }

        echo '<a class="osb-btn" href="' . get_page_link() . '">Map</a>';
    }

    /**
     * Display the "edit" view for a place.
     *
     * @param $id - the id of a place which will be NULL if its a new place.
     * @param $mode - public or contribution (TODO - do we need this since we should be in contribution mode)?
     * @param $invalid - array of parameters that have issues.
     */
    function osb_edit_place_view($id, $mode, $invalid, $state) {

        $data = null;
        $media = null;

        // we are editing an existing form
        if (isset($id)) {

            // get the place details
            $data = $this->_osb_places_db->osb_get_place($id);

            echo '<h2>Edit Place Details</h2>';

            if (!$this->osb_allow_editing($data['status'], $data['user_id'])) {
                echo '<p>Sorry, you cannot edit this content</p>';
                return;
            }

            echo '<p>You can move the location on the map, edit details and
                     add or remove media. You can save as many times as you like. Click \'Done\' when you are
                     ready for the place to be submitted to a moderator for approval.</p>';

            // media for a place
            // $media = $this->_osb_places_db->osb_media_place($id);

        } else {

            echo '<h2>Contribute your story</h2>';
            echo '<p class="osb-form-instruct">Use this form to add a place to the map.</p>';
            echo '<p class="osb-form-instruct">After the first \'save\' you can add keywords and media (e.g. photos).
                  You can \'save\' as many times as you like. Click \'Submit\' when you are ready for the place
                  to be submitted for approval.</p>';

            $data = $this->osb_place_wrap_post_data($this->osb_place_data_wrapper(), $this->_osb_post_data_mapper);
        }

        // display validation issues
        $this->osb_place_form_valid_results($invalid);

        echo '<h3 class="osb-form-header">Location of the place</h3>';
        echo '<p class="osb-form-instruct">To add a point, click on the map or enter the postcode.</p>';
        echo '<p class="osb-form-instruct">After adding a point you can drag it to a different location.
                                           Remove the point by dragging on the map.</p>';
        // postcode lookup
        $this->osb_postcode_lookup();

        // display the map
        $this->osb_map($mode);


        $this->osb_place_form($data, $state);
    }

    /**
     * Helper method to display the form for place details.
     *
     * @param $data - place data (wrapped - the original data could be from the database or a POST submission)
     */
    function osb_place_form($data, $state) {

        # plugin options
        $options = get_option('osb_options');

        ?>
        <div id="osb-place-form" <?php if (!empty($data['id'])) {
            echo ' data-place-id="' . $data['id'] . '"';
        } ?><?php if (!empty($state)) {
            echo ' data-state="' . $state . '"';
        } ?>>

            <form method="POST"
                  action="<?php echo esc_url(add_query_arg($this->_action_param, $this->_contribute_action)) ?>">

                <h3 class="osb-form-header">Details of the place</h3>

                <p class="osb-form-instruct">Please provide further details about the location on the map.
                    <?php if (empty($data['id'])) {
                        echo 'After the first save you can add keywords and media ';
                    } ?></p>

                <?php
                if (isset($data['id'])) {
                    echo '<input id="osb-place-id" type="hidden" name="osb-place-id" value="' . $data['id'] . '"/>';
                }
                ?>
                <div class="osb-place-form-section">
                    <label for="osb-place-title"><strong>Title:</strong> <br/>Enter place name/address and briefly
                        summarise its significance, event, person, etc.</label>
                    <input id="osb-place-title" name="osb-place-title" type="text"
                           value="<?php if (isset ($data['title'])) {
                               echo($data['title']);
                           } ?>"/></div>

                <div class="osb-place-form-section"><strong>Description:</strong><br/>
                    Give details of the place, event, person.
                    <?php
                    $content = isset($data['desc']) ? $data['desc'] : '';
                    if ($options['allow_html_desc'] === 'yes') {
                        wp_editor($content, 'osb-place-desc', array('media_buttons' => false,
                            'tinymce' => array (
                                'toolbar1'=> 'formatselect,bold,italic,bullist,numlist,undo,redo,cut,copy,paste,cleanup',
                                'toolbar2' => '')));
                    } else {
                        echo '<textarea name="osb-place-desc">' . $content . '</textarea>';
                    }
                    ?></div>

                <div id="osb-date-section" class="osb-place-form-section">
                    <p><strong>Date:</strong></p>
                    <p>Provide a <strong>single date</strong> or the <strong>from date</strong> in a date range<br/>

                    Year estimate
                    <select id="osb-place-year-estimate" name="osb-place-year-estimate">
                        <?php
                        foreach (Date::YearEstimate() as $key => $value) { ?>
                            <option
                                value="<?php echo $key ?>"<?php if (isset($data['year_estimate']) &&
                                $data['year_estimate'] == $key
                            ) echo 'selected' ?>><?php echo $value ?></option>
                        <?php } ?>
                    </select>

                    or Year
                    <input id="osb-place-year" name="osb-place-year" size="4" value="<?php if (!empty($data['year'])) {
                        echo $data['year'];
                    } ?>"><br/>

                    Month estimate
                    <select id="osb-place-month-estimate" name="osb-place-month-estimate">
                        <?php
                        foreach (Date::MonthEstimate() as $key => $value) { ?>
                            <option value="<?php echo $key ?>"<?php if (isset($data['month_estimate']) &&
                                $data['month_estimate'] == $key
                            ) echo 'selected' ?>><?php echo $value ?></option>
                        <?php } ?>
                    </select>

                    or Month
                    <select id="osb-place-month" name="osb-place-month">
                        <?php
                        foreach (Date::Month() as $key => $value) { ?>
                            <option
                                value="<?php echo $key ?>"<?php if (isset($data['month']) &&
                                $data['month'] == $key
                            ) echo 'selected' ?>><?php echo $value ?></option>
                        <?php } ?>
                    </select><br/>

                    Day estimate
                    <select id="osb-place-day-estimate" name="osb-place-day-estimate">
                        <?php
                        foreach (Date::DayEstimate() as $key => $value) { ?>
                            <option
                                value="<?php echo $key ?>"<?php if (isset($data['day_estimate']) &&
                                $data['day_estimate'] == $key
                            ) echo 'selected' ?>><?php echo $value ?></option>
                        <?php } ?>
                    </select>

                    or day:
                    <select id="osb-place-day" name="osb-place-day">
                        <option value="">N/A</option>
                        <?php
                        for ($i = 1; $i <= 31; $i++) { ?>
                            <option
                                value="<?php echo $i ?>"<?php if (isset($data['day']) &&
                                $data['day'] == $i
                            ) echo 'selected' ?>><?php echo $i ?></option>
                        <?php } ?>
                    </select>
                    </p>

                    <p>Provide a <strong>to date</strong> if you want a date range:<br/>
                        Year estimate
                        <select id="osb-place-to-year-estimate" name="osb-place-to-year-estimate">
                            <?php
                            foreach (Date::YearEstimate() as $key => $value) { ?>
                                <option
                                    value="<?php echo $key ?>"<?php if (isset($data['to_year_estimate']) &&
                                    $data['to_year_estimate'] == $key
                                ) echo 'selected' ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>

                        or Year
                        <input id="osb-place-to-year" name="osb-place-to-year" size="4" value="<?php if (!empty($data['to_year'])) {
                            echo $data['to_year'];
                        } ?>"><br/>

                        Month estimate
                        <select id="osb-place-to-month-estimate" name="osb-place-to-month-estimate">
                            <?php
                            foreach (Date::MonthEstimate() as $key => $value) { ?>
                                <option value="<?php echo $key ?>"<?php if (isset($data['to_month_estimate']) &&
                                    $data['to_month_estimate'] == $key
                                ) echo 'selected' ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>

                        or Month
                        <select id="osb-place-to-month" name="osb-place-to-month">
                            <?php
                            foreach (Date::Month() as $key => $value) { ?>
                                <option
                                    value="<?php echo $key ?>"<?php if (isset($data['to_month']) &&
                                    $data['to_month'] == $key
                                ) echo 'selected' ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select><br/>

                        Day estimate
                        <select id="osb-place-to-day-estimate" name="osb-place-to-day-estimate">
                            <?php
                            foreach (Date::DayEstimate() as $key => $value) { ?>
                                <option
                                    value="<?php echo $key ?>"<?php if (isset($data['to_day_estimate']) &&
                                    $data['to_day_estimate'] == $key
                                ) echo 'selected' ?>><?php echo $value ?></option>
                            <?php } ?>
                        </select>

                        or day:
                        <select id="osb-place-to-day" name="osb-place-to-day">
                            <option value="">N/A</option>
                            <?php
                            for ($i = 1; $i <= 31; $i++) { ?>
                                <option
                                    value="<?php echo $i ?>"<?php if (isset($data['to_day']) &&
                                    $data['to_day'] == $i
                                ) echo 'selected' ?>><?php echo $i ?></option>
                            <?php } ?>
                        </select>
                    </p>
                </div>

                <div id="osb-keywords-wrapper" style="display: none">
                    <strong>Keywords:</strong><br/>
                    <div id="osb-keyword-place">

                    </div>
                    <div id="osb-keyword">
                        <input class="typeahead" type="text" placeholder="Keyword" id="osb-new-keyword"
                               name="osb-new-keyword"/><input type="submit" id="osb-new-keyword-add" value="Add">
                    </div>
                </div>
                <?php if (isset($data['id'])) { ?>
                    <div id="osb-add-media-wrapper">
                        <h3 class="osb-form-header">Media</h3>
                        <div id="osb-media-wrapper"><p>No media is associated with the place.</p></div>
                        <p>
                            <a href="<?php echo esc_url(add_query_arg(array($this->_action_param => $this->_add_media_action, $this->_place_id_param => $data['id']))) ?>"
                               id="osb-add-media" class="osb-btn-small">Add media</a></p>
                        <p id="osb-media-msg"></p>
                    </div>
                <?php } ?>

                <input id="osb-place-lat" type="hidden" name="osb-place-lat"
                       value="<?php if (isset($data['lat'])) {
                           echo($data['lat']);
                       } ?>"/>
                <input id="osb-place-lon" type="hidden" name="osb-place-lon"
                       value="<?php if (isset($data['lon'])) {
                           echo($data['lon']);
                       } ?>"/>

                <input id="osb-selected-layer" type="hidden" name="osb-selected-layer"
                       value="<?php if (isset($data['osb-selected-layer'])) {
                           echo($data['osb-selected-layer']);
                       } ?>"/>

                <input id="osb-layer-zoom" type="hidden" name="osb-layer-zoom"
                       value="<?php if (isset($data['osb-layer-zoom'])) {
                           echo($data['osb-layer-zoom']);
                       } ?>"/>

                <input id="osb-current-lat" type="hidden" name="osb-current-lat"
                       value="<?php if (isset($data['osb-current-lat'])) {
                           echo($data['osb-current-lat']);
                       } ?>"/>

                <input id="osb-current-lon" type="hidden" name="osb-current-lon"
                       value="<?php if (isset($data['osb-current-lon'])) {
                           echo($data['osb-current-lon']);
                       } ?>"/>

                <div id="osb-submission">
                    <p>
                        <input id="osb-place-submit" class="osb-btn-submit" name="osb-place-submit" type="submit"
                               value="<?php echo $this->_place_save ?>"/>
                        <input id="osb-place-submit-exit" class="osb-btn-submit" name="osb-place-submit" type="submit"
                               value="<?php echo $this->_place_save_exit ?>"/>
                        <input id="osb-place-cancel" class="osb-btn-submit" name="osb-place-submit" type="submit"
                               value="<?php echo $this->_place_exit ?>"/>
                    </p>
                    <?php
                    // certain buttons only appear if we have an ID
                    if (isset($data['id'])) {

                        // if we are in draft we can submit for moderation
                        if (isset($data['status']) && $data['status'] == Status::Draft) {
                            ?><p>
                            <input id="osb-place-submit" class="osb-btn-submit" name="osb-place-submit" type="submit"
                                   value="<?php echo $this->_place_save_submit ?>"/>
                            </p><?php
                        }

                        // if we are a moderator (and we have a status) we can approve or retract
                        if (OsbUtility::osb_is_moderator() && isset($data['status']) && $data['status'] != Status::Draft) {
                            ?><p><?php
                            if ($data['status'] == Status::Awaiting_Approval) {
                                ?><input id="osb-place-submit" class="osb-btn-submit" name="osb-place-submit"
                                         type="submit"
                                         value="<?php echo $this->_place_approve ?>"/><?php
                            }
                            if ($data['status'] == Status::Approved) {
                                ?><input id="osb-place-submit" class="osb-btn-submit" name="osb-place-submit"
                                         type="submit"
                                         value="<?php echo $this->_place_retract ?>"/><?php
                            }
                            ?></p><?php
                        }

                        // if we are a moderator and or the owner, we can delete
                        if (isset($data['user_id'])) {
                            if (OsbUtility::osb_is_moderator() || (isset($data['user_id']) && get_current_user_id() == $data['user_id'])) {
                                ?><p><input id="osb-place-delete" class="osb-btn-submit" name="osb-place-submit"
                                         type="submit"
                                         value="<?php echo $this->_place_delete ?>"/></p><?php
                            }
                        }
                        ?><?php
                    } ?>
                </div>
            </form>

        </div><!-- .osb-place-form-->

        <script type="application/javascript">
            osbMediaPlace.setup();
        </script>

        <?php
    }

    // ---------- MEDIA SUPPORT

    /**
     * Show the media object.
     *
     * @param $place_id - the id of the place.
     * @param $media_id - the id of the media item.
     */
    function osb_show_media($place_id, $media_id) {

        // get the media details
        $data = $this->osb_media_details($media_id);

        // show the media preview
        $this->osb_preview_media($data['id'], $data['type'], $data['url'], $data['mime_type']);

        // display
        $this->osb_media_view($place_id, $media_id, $data['type'],
            $data['post']->post_title, $data['alt'], $data['post']->post_content, $data['post']->post_excerpt);
    }

    /**
     * An initial form that allows a user to select a file from disk.
     *
     * @param $place_id - the ID of the place.
     */
    function osb_media_form($place_id, $msg) {
        ?>

        <h3 class="osb-form-header">Add media</h3>

        <?php
        if (!empty($msg)) {
            echo '<p class="osb-error">' . $msg . '</p>';
        }
        ?>
        <p>Please select a media file. After you have added the file, you will be able to add additional details.</p>
        <?php echo $this->acceptable_files() ?>
        <form id="osb-media-upload" method="post" action="#" enctype="multipart/form-data">
            <input type="file" name="osb_media_upload" id="osb-media-upload" multiple="false"/>
            <input type="hidden" name="osb_place_id" id="osb-place-id" value="<?php echo $place_id ?>"/>
            <?php wp_nonce_field('osb_media_upload', 'osb_media_upload_nonce'); ?>
            <div class="osb-permissions-box">
                <p>To upload content to this site you must be the rights holder of the material or have permission from
                    the
                    rights holder to share the material. If you are unsure if you are the rights holder please see
                    <a href="bufvc.co.uk/copyright-guidance" target="_blank">bufvc.co.uk/copyright-guidance</a>.</p>
                <p>When you upload content to this site, you are licensing your content for use under
                    <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/" target="_blank">Creative Commons
                        Attribution-NonCommercial-ShareAlike 4.0 International License</a></p>
                <p>This means that:</p>
                <ul>
                    <li>You must give appropriate credit, provide a link to the license, and indicate if changes were
                        made.
                        You may do so in any reasonable manner, but not in any way that suggests the licensor endorses
                        you or your use.
                    </li>
                    <li>You may not use the material for commercial purposes.</li>
                    <li>If you remix, transform, or build upon the material, you must distribute your contributions
                        under the same license as the original.
                    </li>
                </ul>
                <p><input type="checkbox" name="osb_permission"> Permission to use</p>
            </div>

            <p><input id="osb-media-upload" name="osb_media_upload" type="submit" value="Add media"/>
                <input id="osb-media-cancel" name="osb_media_cancel" type="submit" value="Cancel"></p>
        </form>
        <?php
    }

    /**
     * A page that displays information about a media asset.
     *
     * @param $place_id - the ID of a place the asset is associated with.
     * @param $media_id - the ID of the media item.
     * @param $type - are we dealing with an image?
     * @param $title - the title of the asset.
     * @param $alt - the alt text of the asset (if an image).
     * @param $desc - the description of the asset.
     * @param $caption - the caption of the asset.
     */
    function osb_media_view($place_id, $media_id, $type, $title, $alt, $desc, $caption) {

        echo '<div id="osb-media-metadata">';

        echo '<p><strong>Title:</strong> ' . $title . '</p>';

        if ($type === 'image') {
            echo '<p><strong>Alt:</strong> ' . $alt . '</p>';
        }

        echo '<p><strong>Caption:</strong> ' . $caption . '</p>';
        echo '<p><strong>Description:</strong> ' . $desc . '</p>';
        echo '<p><a href="' . esc_url(add_query_arg(array($this->_action_param => $this->_contribute_action,
                $this->_place_id_param => $place_id))) . '" class="osb-btn-small">' . $this->_media_view_done . '</a> ';

        $place = $this->_osb_places_db->osb_get_place($place_id);

        if ($this->osb_allow_editing($place['status'], $place['user_id'])) {
            echo '<a href="' . esc_url(add_query_arg(array($this->_action_param => $this->_edit_media_action,
                    $this->_media_id_param => $media_id, $this->_place_id_param => $place_id))) .
                '" class="osb-btn-small">' . $this->_media_view_edit . '</a></p>';
        }

        echo '</div>';
    }

    /**
     * We need a restrictive subset of file formats that the Council will accept.
     *
     * @return array a list of acceptable file types.
     */
    function osb_mime_types() {
        return array(
            // Image formats
            'jpg|jpeg' => 'image/jpeg',
            'png' => 'image/png',
            // Video formats.
            'mpeg|mpg|mpe' => 'video/mpeg',
            'mp4|m4v' => 'video/mp4',
            'mkv' => 'video/x-matroska',
            // audio formats
            'mp3|m4a' => 'audio/mpeg',
            'ogg' => 'audio/ogg',
            // Misc application formats.
            'pdf' => 'application/pdf');
    }

    /**
     * Text with acceptable file formats.
     *
     * @return string - text of acceptable formats.
     */
    function acceptable_files() {
        return '<p>Acceptable files: images (jpg, jpeg and png); audio (mp3, m4a and ogg); video (mpeg, mp4
                and m4k); and miscellaneous (pdf). <strong>Maximum size: 5MB</strong></p>';
    }

    /**
     * Handle the submission of the media object. If all goes well, we show a web form
     * to add metadata.
     */
    function osb_handle_media_submit() {

        if (!empty($_POST['osb_media_cancel']) && sanitize_text_field($_POST['osb_media_cancel']) === $this->_media_cancel) {
            $this->osb_edit_place_view(sanitize_text_field($_POST['osb_place_id']), 'contribute', array());
            return;
        }

        if (isset($_POST['osb_media_upload_nonce'], $_POST['osb_place_id'])
            && wp_verify_nonce(sanitize_text_field($_POST['osb_media_upload_nonce']), 'osb_media_upload')
        ) {

            // ensure that the terms and conditions have been accepted
            if (empty($_POST['osb_permission'])) {
                $this->osb_media_form(sanitize_text_field($_POST['osb_place_id']), "You must accept the terms and conditions");
                return;
            }

            // needed to support media
            require_once(ABSPATH . 'wp-admin/includes/image.php');
            require_once(ABSPATH . 'wp-admin/includes/file.php');
            require_once(ABSPATH . 'wp-admin/includes/media.php');

            add_filter('mime_types', array($this, 'osb_mime_types'));

            // let WordPress handle the upload
            $attachment_id = media_handle_upload('osb_media_upload', 0);

            remove_filter('mime_types', array($this, 'osb_mime_types'));

            if (is_wp_error($attachment_id)) {
                // There was an error uploading the image.
                echo '<div id="message" class="error"><p>' . $attachment_id->get_error_message() . '</p></div>';
                echo '<p><a href="' . esc_url(add_query_arg(array($this->_action_param => $this->_contribute_action,
                        $this->_place_id_param => sanitize_text_field($_POST['osb_place_id'])))) . '" class="osb-btn">Ok</a></p>';
            } else {

                // The image was uploaded successfully!
                echo '<h3 class="osb-form-header">Media Details</h3>';

                // create a reference between the media and place
                $this->_osb_places_db->osb_add_media_place(sanitize_text_field($_POST['osb_place_id']), $attachment_id);

                $this->osb_media_metadata_form_page(sanitize_text_field($_POST['osb_place_id']), $attachment_id, true);
            }

        } else {

            // The security check failed, maybe show the user an error.

            echo "Failed security check";

        }

    }

    /**
     * Show the media preview.
     *
     * @param $media_id - the id of the media object.
     * @param $type - the type of media object.
     * @param $url - the URL of the media item.
     * @param $mine_type - the mime type of the media item.
     */
    function osb_preview_media($media_id, $type, $url, $mine_type) {

        switch ($type) {
            case "image":
                echo wp_get_attachment_image($media_id, 'medium');
                break;
            case "video":
                echo '<p><video controls><source src="' . $url . '" type="' . $mine_type . '"/></video></p>';
                break;
            case "audio":
                echo '<p><audio controls><source src="' . $url . '" type="' . $mine_type . '"/></audio></p>';
                break;
            default:
                echo '<p><a href="' . $url . '">' . $mine_type . '</a></p>';
        }

    }

    /**
     * Get the information we need about media.
     *
     * @param $media_id - the id of the media.
     * @return array - array encapsulating the data.
     */
    function osb_media_details($media_id) {

        $alt = null; // default

        // get the media
        $post = get_post($media_id);

        // get the type
        $type = OsbUtility::osb_file_type($post);
        $url = wp_get_attachment_url($media_id);
        $mine_type = get_post_mime_type($media_id);

        if ($type === 'image') {
            $alt = get_post_meta($media_id, '_wp_attachment_image_alt', true);
        }

        return array('id' => $media_id, 'url' => $url, 'type' => $type, 'mime_type' => $mine_type, 'alt' => $alt,
            'post' => $post);
    }


    /**
     * Display a form to add or edit media metadata.
     *
     * @param $place_id - the place id.
     * @param $media_id - the media id.
     * @param $new = track if newly uploaded. Default is false.
     */
    function osb_media_metadata_form_page($place_id, $media_id, $new = false) {

        // get the media details
        $data = $this->osb_media_details($media_id);

        // show the media preview
        $this->osb_preview_media($data['id'], $data['type'], wp_get_attachment_url($media_id), $data['mime_type']);

        if ($new) {
            echo '<p>The media file has been uploaded to WordPress. Please provide some additional information.</p>';
        }

        // show the media form
        $this->osb_media_metadata_form($place_id, $media_id,
            $data['type'], $data['post']->post_title, $data['alt'], $data['post']->post_content, $data['post']->post_excerpt);

    }

    /**
     * Display a form to capture metadata about an asset.
     *
     * @param $place_id - the ID of a place associated with the media.
     * @param $media_id - the ID of an asset (we store the asset before adding metadata).
     * @param $type - type of asset ('image', 'audio', 'video' or 'other').
     * @param $title - title of the asset.
     * @param $alt - alt of the asset (if an image).
     * @param $desc - description of the asset.
     * @param $caption - caption of the asset.
     */
    function osb_media_metadata_form($place_id, $media_id, $type, $title, $alt, $desc, $caption) {

        ?>
        <form id="osb-media-metadata" action="" method="post">
            <input type="hidden" name="osb_place_id" value="<?php echo $place_id ?>">
            <input type="hidden" name="osb_media_id" value="<?php echo $media_id ?>"/>
            <label for="osb-media-title">Title<br/></label>
            <input type="text" id="osb-media-title" name="osb_media_title" value="<?php echo $title ?>"/>
            <div class="osb-media-desc">Say what the image or media file portrays, e.g. name of place or person and,
                maybe, the date.</div>
            <?php
            if ($type === 'image') {
                echo '<label for="osb-media-alt">Alt text</label>';
                echo '<input type="text" id="osb-media-alt" name="osb_media_alt" value="' . $alt . '"/>';
                echo '<div class="osb-media-desc">&lsquo;Alternative text&lsquo; is displayed in place of an image ' .
                     'if graphics are turned off, or read out aloud by screen readers used by the blind. What should ' .
                     '&lsquo;alt text&lsquo; say? Imagine describing to a blind person what the image is conveying. ' .
                     'Be succinct, ideally between 5 and 15 words.</div>';

            }
            ?>
            <label for="osb-media-desc">Description</label>
            <textarea name="osb_media_desc" id="osb-media-desc"><?php echo $desc ?></textarea>
            <div class="osb-media-desc">Additional information about the media (optional).</div>
            <label for="osb-media-caption">Caption</label>
            <input type="text" id="osb-media-caption" name="osb_media_caption" value="<?php echo $caption ?>"/>
            <div class="osb-media-desc">This text will appear under the image. Include any credits or acknowledgements
                (photographer, owner, etc).</div>
            <div id="osb-submission">
                <input name="osb_media_submit" type="submit" value="<?php echo $this->_media_save ?>"/>
                <input name="osb_media_submit" type="submit" value="<?php echo $this->_media_save_exit ?>"/>
                <input name="osb_media_submit" type="submit" value="<?php echo $this->_media_delete ?>">
            </div>
        </form>
        <?php
    }

    /**
     * Handle the submission of the metadata.
     */
    function osb_handle_media_metadata_submit() {

        // get variables
        $place_id = sanitize_text_field($_POST['osb_place_id']);
        $media_id = sanitize_text_field($_POST['osb_media_id']);

        // throw and error if unexpected
        if (!ctype_digit($place_id) && !ctype_digit($media_id)) {
            $this->controller_error();
            return;
        }

        if (!empty($_POST['osb_media_submit'])) {

            if (sanitize_text_field($_POST['osb_media_submit']) == 'Delete') {

                // delete the media item
                wp_delete_attachment($media_id);

                // return to the place
                $this->osb_edit_place_view($place_id, "contribute", array(), null);

                return;
            }
        }

        // update the asset details
        wp_update_post(array('ID' => $media_id,
            'post_title' => sanitize_text_field(stripslashes_deep($_POST['osb_media_title'])),
            'post_excerpt' => sanitize_text_field(stripslashes_deep($_POST['osb_media_caption'])),
            'post_content' => sanitize_text_field(stripslashes_deep($_POST['osb_media_desc']))));

        // if an image we need to update the alt text
        if (!empty($_POST['osb_media_alt'])) {
            update_post_meta(sanitize_text_field($media_id), '_wp_attachment_image_alt',
                sanitize_text_field(stripslashes_deep($_POST['osb_media_alt'])));
        }

        if (sanitize_text_field($_POST['osb_media_submit']) === $this->_media_save) {
            $this->osb_media_metadata_form_page($place_id, $media_id);
        } else {
            $this->osb_show_media($place_id, $media_id);
        }

    }

    /**
     * Delete the data that references between media and a place.
     *
     * @param $media_id
     */
    function delete_media_place($media_id) {
        $this->_osb_places_db->osb_delete_media($media_id);
    }


    /**
     * Displays a form to support the postcode lookup.
     */
    function osb_postcode_lookup() {

        # plugin options
        $options = get_option('osb_options');

        if (isset($options['postcode_lookup_postcodes_io']) && $options['postcode_lookup_postcodes_io'] == 'yes') {
            ?>
            <div id="osb-point-details">
                <form>
                <p>Search via postcode:
                    <input id="osb-postcode-input" tabindex="1" name="osb-postcode-input"/>
                    <input id="osb-postcode-submit" tabindex="1" size="6" type="submit" value="Lookup">
                    <br/><span id="osb-postcode-msg" class="osb-error"></span>
                </p>
                </form>
            </div>
            <?php
        }
    }

    /**
     * Helper method to display a list of validation issues.
     *
     * @param $invalid - array of parameters that have issues.
     */
    function osb_place_form_valid_results($invalid) {
        if (count($invalid) > 0) {
            ?>
            <div id="osb-validation-messages">
                <p>There are validation errors:</p>
                <ul>
                    <?php
                    foreach ($invalid as $key) {
                        ?>

                        <li><?php echo($this->_osb_place_validation_messages[$key]) ?></li><?php
                    }
                    ?>
                </ul>
            </div>
            <?php
        }
    }

    /**
     * Helper method - we used an associative array to represent form data and data from the database.
     *
     * @return array - an associative array representing a place.
     */
    function osb_place_data_wrapper() {
        return array(
            "id" => null,
            "title" => null,
            "lat" => null,
            "lon" => null,
            "desc" => null,
            "status" => null,
            "user_id" => null,
            "year_estimate" => null,
            "year" => null,
            "month_estimate" => null,
            "month" => null,
            "day_estimate" => null,
            "day" => null,
            "to_year_estimate" => null,
            "to_year" => null,
            "to_month_estimate" => null,
            "to_month" => null,
            "to_day_estimate" => null,
            "to_day" => null
        );
    }

    /**
     * Wrap POST data about a place into an associative array.
     *
     * @param $data_wrapper - this will hold the post data.
     * @param $data_mapper - help work out where the POST data is mapped to in the array.
     * @return array - the post data wrapped.
     */
    function osb_place_wrap_post_data($data_wrapper, $data_mapper) {

        $options = get_option('osb_options');

        $data = $this->osb_place_data_wrapper();

        // no data, displaying an empty form, so send empty data structure
        if (!isset($_POST)) {
            return $data_wrapper;
        }

        // get the variable names
        $form_keys = array_keys($data_mapper);

        // add found post data to the data object
        foreach ($form_keys as $key) {
            if (isset ($_POST[$key])) {
                if ($key === 'osb-place-desc' && $options['allow_html_desc'] === 'yes') {
                    $data[$data_mapper[$key]] = wpautop(stripslashes_deep($_POST[$key]));
                } else {
                    $data[$data_mapper[$key]] = sanitize_text_field(stripslashes_deep($_POST[$key])); // http://stackoverflow.com/questions/7341942
                }
            }
        }

        return $data;
    }


    /**
     * Simple validation checker - values that can't be empty.
     *
     * @param $non_empty_fields - fields that are needed.
     * @return array - fields that are missing.
     */
    function osb_check_input(&$non_empty_fields) {

        $validationIssues = array();

        foreach ($non_empty_fields as $field) {
            if (!isset($_POST[$field]) || empty($_POST[$field])) {
                array_push($validationIssues, $field);
            }
        }

        return $validationIssues;
    }

    /**
     * Date range check - should not have an end date without a start date.
     *
     * @return bool - return true if we have a "to date" but not a "from date"
     */
    function osb_date_range_check() {

        $fields = array('year-estimate', 'year', 'month-estimate', 'month', 'day-estimate', 'day');

        $from_range = false;
        $to_range = false;

        foreach ($fields as $field) {
            if (!empty($_POST['osb-place-' . $field])) {
                $from_range = true;
            }
        }

        foreach ($fields as $field) {
            if (!empty($_POST['osb-place-to-' . $field])) {
                $to_range = true;
            }
        }

        return $to_range == true && $from_range == false;

    }

    /**
     * Map layers.
     *
     * @return string - HTML that shows the layers.
     */
    function osb_layer_list() {

        global $wpdb;

        $selected_layer = "";

        # start layer div
        $out = '<div id="osb-layers">';

        $layer_query = 'SELECT * FROM ' . $wpdb->prefix . $this->_osb_layer_table . ' ORDER BY layer_order;';
        $layer_items = $wpdb->get_results($layer_query, ARRAY_A);

        if (!empty($_POST['osb-selected-layer'])) {
            $selected_layer = sanitize_text_field($_POST['osb-selected-layer']);
        } else {
            if (count($layer_items) > 0) {
                $selected_layer = $layer_items[0]['layer_id'];
            }
        }

        if (!empty($layer_items)) {
            $out .= '<ul id="osb-layer-list">';
            foreach ($layer_items as $layer_item) {
                $out .= '<li data-layer-id="' . $layer_item['layer_id'] . '" data-layer-attr="' .
                    htmlspecialchars($layer_item['layer_attribution'], ENT_QUOTES) .
                    '" data-layer-url="' . htmlspecialchars($layer_item['layer_url'], ENT_QUOTES) . '"';

                if ($layer_item['layer_id'] == $selected_layer) {
                    $out .= ' class="osb-layer-selected"';
                }

                $out .= '><a href="#">' .
                    $layer_item['layer_name'] . '</a></li>';
            }
            $out .= '</ul>';
        } else {
            $out .= '<p>No map layers are defined.</p>';
        }

        # end layers div
        $out .= "</div><!-- .osb-layers -->";

        return $out;
    }

    /**
     * Checks that we are able to contribute.
     *
     * @return string - HTML fragments.
     */
    function osb_contribute_text() {

        global $wp;

        $out = "<p>";

        if (is_user_logged_in()) {
            $out .= '<p style="text-align: center"><a href="' . esc_url(add_query_arg($this->_action_param, $this->_contribute_action)) . '" class="osb-btn">Contribute</a>';
        } else {
            $out .= 'You must be <strong><a href="' . wp_login_url(add_query_arg(NULL, NULL)) . '">logged in</a></strong> to contribute.';
        }

        $out .= "</p>";

        return $out;
    }

    function osb_intro_text() {
        ?>
        <p>The contributions added to this map are displayed on these websites:</p>
        <ul>
            <li><em><a href="http://maps.bristol.gov.uk/kyp/">Know Your Place</a></em> provided by Bristol City Council and
                adjacent local authorities.</li>
            <li>OutStories Bristol – a community group that records the lives of LGBT+ people in the Bristol region.</li>
            <li>OutStories Mapping LGBT+ Bristol mobile app (Android and iPhone).</li>
        </ul>
        <p>OutStories Bristol curates the 'LGBT Life' layer on <em>Know Your Place</em> for Bristol City Council and
            other local authorities. Your contribution will be subject to approval by <em>Know Your Place</em>.
            Login/registration is required to contribute. Your login name will appear with your submission on the map.
            Your email address will be confidential. It will not be used for any purpose other than if we need to
            contact you about your submission. It will not be passed to other organisations.</p>
        <?php
    }


    /**
     * Can the logged in user edit the place?
     *
     * @param $status
     * @param $owner_id
     * @return bool
     */
    function osb_allow_editing($status, $owner_id) {

        //  can always edit
        if (OsbUtility::osb_is_moderator()) {
            return true;
        }

        // not yet moderated, and the owner is the logged in user
        if (($status == Status::Draft || $status = Status::Awaiting_Approval) && get_current_user_id() == $owner_id) {
            return true;
        }

        // content moderated, do we allow changes?
        $options = get_option('osb_options');
        if ($status == Status::Approved && get_current_user_id() === $owner_id && $options['edit_after_moderation'] === 'yes') {
            return true;
        }

        return false;
    }


// ---------- ACTIVATION AND DEACTIVATION

    function osb_activate_plugin() {

        // activate the plugin ...
        $this->osb_set_default_options();

        global $wpdb;
        $this->osb_setup_layers_db($wpdb->prefix);
        $this->_osb_places_db->osb_setup_db();
        $this->_osb_keywords_db->osb_setup_db();
    }

    function osb_deactivate_plugin() {

        // deactivate the plugin ...

    }

    function osb_set_default_options() {

        if (get_option('osb_options') === false) {
            $new_options['default_latitude'] = "51.450521";
            $new_options['default_longitude'] = "-2.594694";
            $new_options['default_zoom'] = 17;
            $new_options['layer_placement'] = 'above';
            $new_options['display_unmoderated_content'] = 'yes';
            $new_options['edit_after_moderation'] = 'yes';
            $new_options['postcode_lookup_postcodes_io'] = 'yes';
            $new_options['allow_html_desc'] = 'no';
            add_option('osb_options', $new_options);
        }

    }

    function osb_setup_layers_db($prefix) {

        $table_name = $prefix . $this->_osb_layer_table;

        $create_table =
            'CREATE TABLE IF NOT EXISTS ' . $table_name . ' (
             id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
             layer_id VARCHAR(20) NOT NULL UNIQUE,
             layer_name VARCHAR(30) NOT NULL,
             layer_url VARCHAR(200) NOT NULL,
             layer_attribution TEXT NOT NULL,
             layer_order INT NOT NULL DEFAULT 0
            );';

        global $wpdb;

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($create_table);

        $count = $wpdb->get_var('SELECT COUNT(*) FROM ' . $table_name);

        if ($count == 0) {

            $wpdb->insert(
                $table_name,
                array(
                    'layer_id' => 'stamen',
                    'layer_name' => 'Stamen Toner',
                    'layer_url' => 'http://tile.stamen.com/toner/{z}/{x}/{y}.png',
                    'layer_attribution' => 'Map tiles by <a href=\'http://stamen.com\'>Stamen Design</a>, under <a href=\'http://creativecommons.org/licenses/by/3.0\'>CC BY 3.0</a>. Data by <a href=\'http://openstreetmap.org\'>OpenStreetMap</a>, under <a href=\'http://www.openstreetmap.org/copyright\'>ODbL</a>.',
                    'layer_order' => 1
                )
            );

            $wpdb->insert(
                $table_name,
                array(
                    'layer_id' => 'bro1949',
                    'layer_name' => '1949',
                    'layer_url' => 'http://maps.bristol.gov.uk/ArcGIS/rest/services/BaseMaps/1949_epsg3857/MapServer/tile/{z}/{y}/{x}/',
                    'layer_attribution' => '&copy; <a href=\'http://www.bristol.gov.uk/page/leisure-and-culture/records-and-archives\'>Bristol Record Office</a>',
                    'layer_order' => 2
                )
            );

            $wpdb->insert(
                $table_name,
                array(
                    'layer_id' => 'bro1900',
                    'layer_name' => '1900',
                    'layer_url' => 'http://maps.bristol.gov.uk/ArcGIS/rest/services/BaseMaps/bcc_cs_e2_col_epsg3857/MapServer/tile/{z}/{y}/{x}/',
                    'layer_attribution' => '&copy; <a href=\'http://www.bristol.gov.uk/page/leisure-and-culture/records-and-archives\'>Bristol Record Office</a>',
                    'layer_order' => 3
                )
            );

            $wpdb->insert(
                $table_name,
                array(
                    'layer_id' => 'bro1828',
                    'layer_name' => '1828 Ashmead',
                    'layer_url' => 'http://maps.bristol.gov.uk/ArcGIS/rest/services/BaseMaps/1828_epsg3857/MapServer/tile/{z}/{y}/{x}/',
                    'layer_attribution' => '&copy; <a href=\'http://www.bristol.gov.uk/page/leisure-and-culture/records-and-archives\'>Bristol Record Office</a>',
                    'layer_order' => 4
                )
            );
        }
    }


}
