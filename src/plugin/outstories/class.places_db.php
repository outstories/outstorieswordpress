<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

class places_db {

    // table names
    private $_osb_place_table = "osb_place_data";
    private $_osb_place_media = "osb_place_media";
    private $_osb_keywords_place = "osb_keywords_place";

    // use the web mercator value
    private $_default_srid = "900913";

    function __construct() {
    }

    // ---------- SETUP

    /**
     * Create a table for holding data about places.
     */
    function osb_setup_db() {

        global $wpdb;

        $create_table =
            'CREATE TABLE ' . $wpdb->prefix . $this->_osb_place_table . ' (
             id INT(11) NOT NULL AUTO_INCREMENT,
             title VARCHAR(100) NOT NULL,
             description LONGTEXT NOT NULL,
             coord POINT NOT NULL,
             user_id INT(11) NOT NULL,
             status BOOLEAN NOT NULL DEFAULT 0,
             year_estimate VARCHAR(10) DEFAULT NULL,
             year VARCHAR(4) DEFAULT NULL,
             month_estimate VARCHAR(5) DEFAULT NULL,
             month  VARCHAR(2) DEFAULT NULL,
             day_estimate VARCHAR(5) DEFAULT NULL,
             day VARCHAR(2) DEFAULT NULL,
             to_year_estimate VARCHAR(10) DEFAULT NULL,
             to_year VARCHAR(4) DEFAULT NULL,
             to_month_estimate VARCHAR(5) DEFAULT NULL,
             to_month  VARCHAR(2) DEFAULT NULL,
             to_day_estimate VARCHAR(5) DEFAULT NULL,
             to_day VARCHAR(2) DEFAULT NULL,
             PRIMARY KEY  (id)
            );';


        $create_place_media_table =
            'CREATE TABLE ' . $wpdb->prefix . $this->_osb_place_media . ' (
             place_id INT(11) NOT NULL,
             media_id BIGINT(20) unsigned NOT NULL,
             PRIMARY KEY  (place_id,media_id)
             )';

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($create_table);
        dbDelta($create_place_media_table);

    }

    // ---------- TEAR DOWN


    /**
     * Delete the table holding place data.
     */
    function osb_tear_down_db() {

        global $wpdb;

        $drop_place_table = 'DROP TABLE IF EXISTS ' . $wpdb->prefix . $this->_osb_place_table . ';';

        $wpdb->query($drop_place_table);

        $drop_media_table = 'DROP TABLE IF EXISTS ' . $wpdb->prefix . $this->_osb_place_media . ';';

        $wpdb->query($drop_media_table);
    }


    // ---------- CRUD

    /**
     * Get a list of all places.
     *
     * @return array of all places
     */
    public function find_all_places() {

        // get the plugin options
        $options = get_option('osb_options');

        global $wpdb;

        // initiate variables
        $data = array();
        $places = null;

        // base sql
        $places_query = 'SELECT id, title, description, x(coord) AS lat, y(coord) AS lon, status, user_id, ' .
            'year_estimate, year, month_estimate, month, day_estimate, day, to_year_estimate, to_year, ' .
            'to_month_estimate, to_month, to_day_estimate, to_day FROM ' . $wpdb->prefix . $this->_osb_place_table;

        // execute sql - use prepared statement if needed
        if ($options['display_unmoderated_content'] === 'no' && !OsbUtility::osb_is_moderator()) {
            $places_query .= ' where status =  %d or user_id = %d';
            $places = $wpdb->get_results($wpdb->prepare($places_query, Status::Approved, get_current_user_id()),
                ARRAY_A);
        } else {
            $places = $wpdb->get_results($places_query, ARRAY_A);
        }

        // encapsulate the data ...
        foreach ($places as $place) {
            array_push($data, $this->wrap_place_data($place));
        }

        return $data;
    }

    /**
     * A list of places that have been approved by a moderator.
     *
     * @return array - a list of approved places.
     */
    function all_moderated_places() {

        global $wpdb;

        $data = array();

        // get all places that have been approved
        $places_query = 'SELECT id, title, description, x(coord) AS lat, y(coord) AS lon, user_id, ' .
            'year_estimate, year, month_estimate, month, day_estimate, day, to_year_estimate, to_year, ' .
            'to_month_estimate, to_month, to_day_estimate, to_day, status FROM ' .
            $wpdb->prefix . $this->_osb_place_table . ' WHERE status = %d';

        // run the query
        $places = $wpdb->get_results($wpdb->prepare($places_query, Status::Approved), ARRAY_A);

        // encapsulate the data ...
        foreach ($places as $place) {
            array_push($data, $this->wrap_place_data($place));
        }

        return $data;
    }


    /**
     * Get the details of a single place.
     *
     * @param $id - the ID of the place.
     * @return array - the results.
     */
    public function osb_get_place($id) {

        global $wpdb;

        $select_place = 'SELECT id, title, description, X(coord) as lat, Y(coord) as lon, user_id, status, ' .
            'year_estimate, year, month_estimate, month, day_estimate, day, to_year_estimate, to_year, ' .
            'to_month_estimate, to_month, to_day_estimate, to_day FROM ' . $wpdb->prefix . $this->_osb_place_table .
            ' WHERE id = %d';

        $place_data = $wpdb->get_row($wpdb->prepare($select_place, $id), ARRAY_A);

        return $this->wrap_place_data($place_data);
    }

    /**
     * Wrap the data.
     *
     * @param $place
     * @return array
     */
    function wrap_place_data($place) {

        return array(
            'id' => $place['id'],
            'title' => $place['title'],
            'desc' => $place['description'],
            'lat' => $place['lat'],
            'lon' => $place['lon'],
            'status' => $place['status'],
            'user_id' => $place['user_id'],
            'year_estimate' => $place['year_estimate'],
            'year' => $place['year'],
            'month_estimate' => $place['month_estimate'],
            'month' => $place['month'],
            'day_estimate' => $place['day_estimate'],
            'day' => $place['day'],
            'to_year_estimate' => $place['to_year_estimate'],
            'to_year' => $place['to_year'],
            'to_month_estimate' => $place['to_month_estimate'],
            'to_month' => $place['to_month'],
            'to_day_estimate' => $place['to_day_estimate'],
            'to_day' => $place['to_day']
        );
    }

    /**
     * Insert or update a place.
     *
     * @param $data
     * @return int|mixed
     */
    public function osb_save_place($data) {
        return isset($data['id']) ? $this->osb_update_place($data) : $this->osb_add_place($data);
    }

    /**
     * Add the details of the new place/
     *
     * @param $data - the data regarding a place.
     * @return int - the ID of the new place.
     */
    public function osb_add_place($data) {

        // handle the 'NULL' vs NULL issue
        add_filter('query', array($this, 'wp_db_null_value'));

        global $wpdb;

        // get the date information or null
        $_year_estimate = !empty($data['year_estimate']) ? $data['year_estimate'] : 'NULL';
        $_year = !empty($data['year']) ? $data['year'] : 'NULL';
        $_month_estimate = !empty($data['month_estimate']) ? $data['month_estimate'] : 'NULL';
        $_month = !empty($data['month']) ? $data['month'] : 'NULL';
        $_day_estimate = !empty($data['day_estimate']) ? $data['day_estimate'] : 'NULL';
        $_day = !empty($data['day']) ? $data['day'] : 'NULL';
        $_to_year_estimate = !empty($data['to_year_estimate']) ? $data['to_year_estimate'] : 'NULL';
        $_to_year = !empty($data['to_year']) ? $data['to_year'] : 'NULL';
        $_to_month_estimate = !empty($data['to_month_estimate']) ? $data['to_month_estimate'] : 'NULL';
        $_to_month = !empty($data['to_month']) ? $data['to_month'] : 'NULL';
        $_to_day_estimate = !empty($data['to_day_estimate']) ? $data['to_day_estimate'] : 'NULL';
        $_to_day = !empty($data['to_day']) ? $data['to_day'] : 'NULL';

        // insert sql
        $insert_place = 'INSERT INTO ' . $wpdb->prefix . $this->_osb_place_table .
            ' (title, description, coord, user_id, year_estimate, year, month_estimate, month, day_estimate, day, ' .
            'to_year_estimate, to_year, to_month_estimate, to_month, to_day_estimate, to_day) ' .
            'VALUES (%s, %s, GeomFromText(\'POINT(%f %f)\', %s), %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)';

        // insert via prepare
        $wpdb->query($wpdb->prepare($insert_place, $data["title"], $data["desc"], $data["lat"], $data["lon"],
            $this->_default_srid, $data['user_id'], $_year_estimate, $_year, $_month_estimate, $_month, $_day_estimate,
            $_day, $_to_year_estimate, $_to_year, $_to_month_estimate, $_to_month, $_to_day_estimate, $_to_day));

        remove_filter('query', array($this, 'wp_db_null_value'));

        // id of the inserted place
        return $wpdb->insert_id;
    }

    /**
     * Update a place via a prepared statement.
     *
     * @param $data - data related to a place.
     * @return mixed - the ID of the updated place.
     */
    public function osb_update_place($data) {

        // handle the 'NULL' vs NULL issue
        add_filter('query', array($this, 'wp_db_null_value'));

        global $wpdb;

        // get the date information or null
        $_year_estimate = !empty($data['year_estimate']) ? $data['year_estimate'] : 'NULL';
        $_year = !empty($data['year']) ? $data['year'] : 'NULL';
        $_month_estimate = !empty($data['month_estimate']) ? $data['month_estimate'] : 'NULL';
        $_month = !empty($data['month']) ? $data['month'] : 'NULL';
        $_day_estimate = !empty($data['day_estimate']) ? $data['day_estimate'] : 'NULL';
        $_day = !empty($data['day']) ? $data['day'] : 'NULL';
        $_to_year_estimate = !empty($data['to_year_estimate']) ? $data['to_year_estimate'] : 'NULL';
        $_to_year = !empty($data['to_year']) ? $data['to_year'] : 'NULL';
        $_to_month_estimate = !empty($data['to_month_estimate']) ? $data['to_month_estimate'] : 'NULL';
        $_to_month = !empty($data['to_month']) ? $data['to_month'] : 'NULL';
        $_to_day_estimate = !empty($data['to_day_estimate']) ? $data['to_day_estimate'] : 'NULL';
        $_to_day = !empty($data['to_day']) ? $data['to_day'] : 'NULL';

        // update sql
        $update_place = 'UPDATE ' . $wpdb->prefix . $this->_osb_place_table .
            ' SET title = %s, description = %s, coord = GeomFromText(\'POINT(%f %f)\', %s), year_estimate = %s,' .
            'year = %s, month_estimate = %s, month = %s, day_estimate = %s, day = %s, ' .
            'to_year_estimate = %s, to_year = %s, to_month_estimate = %s, to_month = %s, to_day_estimate = %s, ' .
            'to_day = %s WHERE id = %d';

        // update via prepare
        $wpdb->query($wpdb->prepare($update_place, $data['title'], $data['desc'], $data['lat'], $data['lon'],
            $this->_default_srid, $_year_estimate, $_year, $_month_estimate, $_month, $_day_estimate, $_day,
            $_to_year_estimate, $_to_year, $_to_month_estimate, $_to_month, $_to_day_estimate, $_to_day, $data['id']));

        remove_filter('query', array($this, 'wp_db_null_value'));

        // return the id
        return $data['id'];
    }

    /**
     * Delete a place (and associations with keywords)
     *
     * @param $id - the ID of a place
     */
    public function osb_delete_place($id) {
        global $wpdb;

        // delete keywords associated with the place
        $wpdb->delete($wpdb->prefix . $this->_osb_keywords_place, array('place_id' => $id), array('%d'));

        // remove media regerences
        $this->osb_delete_all_media_place($id);

        // delete the place
        $wpdb->delete($wpdb->prefix . $this->_osb_place_table, array('id' => $id), array('%d'));
    }

    /**
     * Change the publication 'status' of a place.
     *
     * @param $id - the ID of a place.
     * @param $status - the publication status.
     */
    public function osb_change_status($id, $status) {

        global $wpdb;

        $status_place = 'UPDATE ' . $wpdb->prefix . $this->_osb_place_table .
            ' SET status = %d WHERE id = %d';

        $wpdb->query($wpdb->prepare($status_place, $status, $id));
    }

    /**
     * Get the IDs of the media associated with a specific place.
     *
     * @param $place_id - the ID of the place.
     * @return array|null|object
     */
    public function osb_media_place($place_id) {

        global $wpdb;

        $query = 'SELECT DISTINCT media_id FROM ' . $wpdb->prefix . $this->_osb_place_media . ' WHERE place_id = %d';

        return $wpdb->get_results($wpdb->prepare($query, $place_id), ARRAY_A);
    }

    /**
     * Associate a media item with a place.
     *
     * @param $place_id - the ID of a place.
     * @param $media_id - the ID of a media object.
     */
    public function osb_add_media_place($place_id, $media_id) {

        global $wpdb;

        $wpdb->insert(
            $wpdb->prefix . $this->_osb_place_media,
            array(
                'place_id' => $place_id,
                'media_id' => $media_id
            ),
            array(
                '%d',
                '%d'
            )
        );
    }

    /**
     * Get all media items.
     *
     * @return array|null|object
     */
    public function osb_media_all() {

        global $wpdb;

        return $wpdb->get_results('SELECT DISTINCT media_id FROM ' . $wpdb->prefix . $this->_osb_place_media);
    }

    /**
     * Disassociate a media item with a place.
     *
     * @param $place_id - the ID of a place.
     * @param $media_id - the ID of a media object.
     */
    public function osb_delete_media_place($place_id, $media_id) {
        global $wpdb;

        $wpdb->delete(
            $wpdb->prefix . $this->_osb_place_media,
            array(
                'place_id' => $place_id,
                'media_id' => $media_id
            ),
            array(
                '%d',
                '%d'
            )
        );
    }

    /**
     * Disassociate all media items for a place.
     *
     * @param $place_id - the ID of a place.
     */
    public function osb_delete_all_media_place($place_id) {
        global $wpdb;

        $wpdb->delete(
            $wpdb->prefix . $this->_osb_place_media,
            array(
                'place_id' => $place_id
            ),
            array(
                '%d'
            )
        );
    }

    /**
     * Delete a media object (reference).
     *
     * @param $media_id - a media ID.
     */
    public function osb_delete_media($media_id) {
        global $wpdb;

        $wpdb->delete(
            $wpdb->prefix . $this->_osb_place_media,
            array(
                'media_id' => $media_id
            ),
            array(
                '%d'
            )
        );
    }

    /**
     * The wpdb converts references to NULL to a string, i.e. 'NULL'. Using a
     * filter we can alter the SQL to return it to NULL.
     *
     * @param $query
     * @return mixed
     */
    function wp_db_null_value($query) {
        return str_ireplace("'NULL'", "NULL", $query);
    }

}