<?php

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

define('OSB__PLUGIN_DIR', plugin_dir_path(__FILE__));
require_once(OSB__PLUGIN_DIR . 'class.places_db.php');
require_once(OSB__PLUGIN_DIR . 'class.keywords_db.php');

/**
 * Class OSB_Uninstall
 */
class OSB_Uninstall {

    // objects that give access to the database
    private $_osb_places_db;
    private $_osb_keywords_db;
    private $_osb_layer_table = "osb_layer_data";

    /**
     * OSB_Uninstall constructor.
     */
    function __construct() {
        $this->_osb_places_db = new places_db();
        $this->_osb_keywords_db = new keywords_db();
    }

    /**
     * Uninstall the plugin.
     */
    function uninstall() {

        if (get_option('osb_options') !== false) {
            delete_option('osb_options');
        }

        global $wpdb;

        // remove media
        $media = $this->_osb_places_db->osb_media_all();
        foreach ($media as $item) {
            wp_delete_attachment($item->media_id);
        }

        // remove the place data
        $this->_osb_places_db->osb_tear_down_db();

        // remove the keyword data
        $this->_osb_keywords_db->osb_tear_down_db();

        // remove the layers data
        $this->osb_tear_down_layers_db();
    }

    /**
     * Drop the layer database.
     */
    function osb_tear_down_layers_db() {

        global $wpdb;

        $drop_table = 'DROP TABLE IF EXISTS ' . $wpdb->prefix . $this->_osb_layer_table . ';';

        $wpdb->query($drop_table);
    }

}

if (!defined('WP_UNINSTALL_PLUGIN'))
    exit;

$ui = new OSB_Uninstall();
$ui->uninstall();