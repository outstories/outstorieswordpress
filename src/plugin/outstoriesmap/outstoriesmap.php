<?php
/*
  Plugin Name:     OutStoriesMap
  Plugin URI:      https://bitbucket.org/outstories/outstorieswordpressplugin#README
  Description:     Plugin for the AHRC-funded Outstories project. Shows data from Know Your Place.
  Version:         1.0.1
  Author:          Research IT, University of Bristol
  Author URI:      http://www.bristol.ac.uk/it-services/research/
  Licence:         GPLv2
  License URI:     https://www.gnu.org/licenses/gpl-2.0.html
*/

// no direct access to the file
defined('ABSPATH') or die("Sorry, no direct access to the file.");

// ensure we are in a WordPress Blog
if (!defined('WP_PLUGIN_DIR')) {
    die('This is a WordPress plugin - I need WordPress!');
}

define('OSB_MAP_PLUGIN_URL', plugin_dir_url(__FILE__));
define('OSB_MAP_PLUGIN_DIR', plugin_dir_path(__FILE__));



$osb_kyp_map = new OSB_KYP_Map();

class OSB_KYP_Map {

    /**
     *
     * OSB_KYP_Map constructor.
     */
    function __construct() {


        // make sure scripts are available
        add_action('wp_enqueue_scripts', array($this, 'osb_custom_scripts'));

        // the map appears via a short code
        add_shortcode('osb_kyp_map', array($this, 'osb_kyp_short_code'));

    }

    // ----------- PLUGIN SET UP

    /**
     * Register scripts and style sheets wih WordPress.
     */
    function osb_custom_scripts() {

        // make sure we have access to jquery
        wp_enqueue_script('jquery');

        // register the map leaflet javascript
        wp_register_script('leaflet-script', 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js');
        wp_register_style('leaflet-style', 'http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css');
        wp_enqueue_script('leaflet-script');
        wp_enqueue_style('leaflet-style');

        wp_register_style('leaflet-cluster-style', OSB_MAP_PLUGIN_URL . 'css/MarkerCluster.css');
        wp_register_style('leaflet-cluster-style-2', OSB_MAP_PLUGIN_URL . 'css/MarkerCluster.Default.css');
        wp_enqueue_style('leaflet-cluster-style');
        wp_enqueue_style('leaflet-cluster-style-2');

        // register plugin css
        wp_register_style('osb-kyp-map-style', plugins_url('/css/osbjs.css', __FILE__));
        wp_enqueue_style('osb-kyp-map-style');

    }


    function osb_kyp_short_code($atts) {

        ob_start();

        // get the map html
        include 'map.php';

        // JavaScript needed just where we are using the map
        ?>
        <script type="application/javascript" src="<?php echo OSB_MAP_PLUGIN_URL ?>js/leaflet.js"></script>
        <script type="application/javascript" src="<?php echo OSB_MAP_PLUGIN_URL ?>js/leaflet.markercluster.js"></script>
        <script type="application/javascript" src="<?php echo OSB_MAP_PLUGIN_URL ?>js/osb_layers.js"></script>
        <script type="application/javascript" src="<?php echo OSB_MAP_PLUGIN_URL ?>js/osbjs.js"></script>

        <style>


            #osb-map-wrapper {
                height: 800px;
            }
            #osb-map-header {
                line-height: 0;
                height: 30px;
            }

        </style>

        <script type="application/javascript">
            var osb_map = L.map('osb-map').setView([51.450579, -2.594704], 16);

            var icon = L.icon({
                iconUrl: '<?php echo OSB_MAP_PLUGIN_URL ?>image/marker.png',
                iconRetinaUrl: '<?php echo OSB_MAP_PLUGIN_URL ?>image/marker@2x.png',
                iconSize: [32, 47],
                iconAnchor: [16, 46],
                popupAnchor: [0, -55]
            });

            osb.init(mapper, osb_map, osb_layers.layers, icon);
        </script>
        <?php

        return ob_get_clean();
    }

}