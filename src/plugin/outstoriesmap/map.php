<!-- The DIV for the map -->
<div id="osb-map-page-wrapper">
    <div id="osb-map-header">
        <p id="osb-map-header-title">Mapping LGBT+ Bristol <span id="osb-map-toggle" class="osb-map-min" data-state="min"></span></p>
        <p id="osb-map-header-subtitle">Explore our history and contribute your own stories</p>
    </div>
    <div id="osb-map-description">
        <p>This map allows you to explore Bristol's LGBT+ history through a multilayered digital map of the city.
            On the map you can listen to people's stories, view historic photographs, documents, posters and flyers
            and read about places and events of significance to LGBT+ life. You can also add your own stories and
            upload material. The map was created as part the AHRC-funded 'Mapping LGBT+ Bristol' project, a
            collaboration between between OutStories Bristol, the University of Bristol and Bristol City Council.
            The map content is also available on the <a href="http://maps.bristol.gov.uk/kyp/">Know Your Place site</a>.</p>
    </div>
    <div id="osb-map-wrapper" class="filter-x">
        <div id="osb-map-page-right">
            <div id="osb-map-filter">
                <div id="osb-map-filter-header">
                    <p>Filter <span class="osb-filter-close">[-]</span><span class="osb-filter-open">[+]</span></p>
                </div>
                <div id="osb-map-filter-content">
                    <div id="osb-places-total"><p></p></div>
                    <p>Select a base map:</p>
                    <div id="osb-layer-list">
                        <ul></ul>
                    </div>
                    <div id="osb-selected-keyword-list">
                        <p>Filter by keyword:</p>
                        <ul></ul>
                    </div>
                    <div id="osb-keyword-list">
                        <ul></ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="osb-map-page-left" class="osb-map-normal">
            <div id="osb-map" class="osb-map"></div>
            <p id="osb-contribute-map"><a href="/contribute/" class="osb-btn">Contribute</a> to the map</p>
            <div id="osb-logo">
                <ul>
                    <li><img src="<?php echo OSB_MAP_PLUGIN_URL . 'image/osb-logo.png' ?>" alt="OutStories Bristol"/></li>
                    <li><img src="<?php echo OSB_MAP_PLUGIN_URL . 'image/uob-logo.png' ?>" alt="University of Bristol"/></li>
                    <li><img src="<?php echo OSB_MAP_PLUGIN_URL . 'image/BCC-logo.png' ?>" alt="Bristol City Council"/></li>
                    <li><img src="<?php echo OSB_MAP_PLUGIN_URL . 'image/ahrc-logo.png' ?>" alt="Arts and Humanities Research Council"/></li>
                </ul>
            </div>
        </div>
    </div>
    <div id="osb-place-details" data-id="" style="display: none">
        <div>
            <div id="osb-place-description">
                <!-- <span class="osb-desc-close-wrapper">[ <a class="osb-desc-close" href="#">x</a> ]</span> -->
                <div id="osb-place-description-content">
                    <div class="spinner medium_wbg"></div>
                </div>
                <div id="osb-place-media"></div>
                <div><p><a class="osb-desc-close osb-btn" href="#">Back to map</a></p></div>
            </div>
        </div>
    </div>
    <!--<div id="osb-map-message" style="display: none"><p>Loading ...</p></div>-->
</div>
