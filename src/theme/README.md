** Background image **

Appearance -> Background -> Background image

	* Background Repeat: No repeat
	* Background Position: Center
	* Background Attachment: Fixed

/* Use 'cover' for the background image
body.custom-background {
	background-size: cover;
}


