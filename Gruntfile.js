module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        app: {
            name: 'outstories-wp-plugin'
        },
        properties: {
            cfg: 'config.properties'
        },
        sass: {
            dist: {
                options: {
                  sourcemap: 'none'
                },
                files: {
                    'src/plugin/outstories/css/osb.css': 'src/scss/main.scss'
                }
            }
        },
        jshint: {
            all: {
                src: ['Gruntfile.js', 'src/plugin/outstories/js/**/*.js', '!src/plugin/outstories/js/lib/**/*.js']
            }
        },
        copy: {
            copy_dev: {
                files: [
                    {
                        cwd: 'src/plugin',
                        src: ['outstories/**', '!**/*.map'],
                        dest: '<%= cfg.pluginDir %>',
                        expand: true
                    },
                    {   cwd: 'src/plugin',
                        src: ['outstoriesmap/**', '!**/*.map'],
                        dest: '<%= cfg.pluginDir %>',
                        expand: true
                    }
                ]
            },
            copy_wp_assets: {
                files: [
                    {
                        expand: true,
                        src: ['<%= cfg.mapSrcDir %>/css/**'],
                        dest: 'src/plugin/outstoriesmap/css',
                        filter: 'isFile',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['<%= cfg.mapSrcDir %>/image/**'],
                        dest: 'src/plugin/outstoriesmap/image',
                        filter: 'isFile',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['<%= cfg.mapSrcDir %>/js/**'],
                        dest: 'src/plugin/outstoriesmap/js',
                        filter: 'isFile',
                        flatten: true
                    },
                    {
                        expand: true,
                        src: ['<%= cfg.mapSrcDir %>/map.php'],
                        dest: 'src/plugin/outstoriesmap/',
                        filter: 'isFile',
                        flatten: true
                    }
                ]
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass'],
                options: {
                    cwd: 'src/scss'
                }
            },
            files: {
                files: ['**/*.php', '**/*.css', '**/*.js'],
                tasks: ['dev-newer'],
                options: {
                    cwd: 'src/plugin'
                }
            }
        },
        compress: {
            contribute: {
                options: {
                    archive: 'outstories.' + '<%= cfg.contribute %>' + '.zip',
                    mode: 'zip'
                },
                files: [
                    {  expand: true, cwd: 'src/plugin', src : ['outstories/**'] }
                ]
            },
            kyp: {
                options: {
                    archive: 'outstories-kyp.' + '<%= cfg.kyp %>' + '.zip',
                    mode: 'zip'
                },
                files: [
                    {  expand: true, cwd: 'src/plugin', src : ['outstoriesmap/**'] }
                ]
            }
        }
    });

    // compression
    grunt.loadNpmTasks('grunt-contrib-compress');

    // sass for css
    grunt.loadNpmTasks('grunt-contrib-sass');

    // we can use a properties file
    grunt.loadNpmTasks('grunt-properties-reader');

    // check our JavaScript
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // copy our files into place
    grunt.loadNpmTasks('grunt-contrib-copy');

    // watch files
    grunt.loadNpmTasks('grunt-contrib-watch');

    // sync newer files (when watching)
    grunt.loadNpmTasks('grunt-newer');

    // default task(s).
    grunt.registerTask('default', ['properties', 'sass', 'jshint']);

    // copy wp dev environment
    grunt.registerTask('dev', ['default', 'copy:copy_dev']);

    // copy changed wp dev environment
    grunt.registerTask('dev-newer', ['properties', 'newer:copy:copy_dev']);

    // create tar for contribution
    grunt.registerTask('dist-cont', ['default', 'compress:contribute']);

    // create tar for the kyp map
    grunt.registerTask('dist-kyp', ['default', 'compress:kyp']);

    // copy files from OutStoriesJS
    grunt.registerTask('wp_assets', ['properties', 'copy:copy_wp_assets']);

};